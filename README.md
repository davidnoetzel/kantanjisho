# Introduction #

Kantan Jisho is a modern Japanese-English dictionary free of feature creep, which pairs a simple, straightforward interface with fast, powerful searching. It is written in C++ using the Qt framework and is designed to be used on any desktop operating system.

Kantan Jisho started out life as my senior project at Yale. For a deeper understanding of the project, read the original [proposal](https://www.dropbox.com/s/8et6uezpffubg0k/CPSC%20490%20Proposal.pdf) and the [final report](https://www.dropbox.com/s/zdx4psq34kmfswr/CPSC%20490%20Report.pdf).

# Dependencies #

* Qt5.4