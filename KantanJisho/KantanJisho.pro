#-------------------------------------------------
#
# Project created by QtCreator 2015-02-08T17:43:22
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += sql
QT       += xml
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KantanJisho
TEMPLATE = app

CONFIG += c++11

SOURCES += src/main.cc \
    src/ui/mainwindow.cc \
    src/search/entry.cc \
    src/search/wordentry.cc \
    src/search/search.cc \
    src/flatten/flattener.cc \
    src/flatten/defaulthandlerfordb.cc \
    src/flatten/wordhandler.cc \
    src/flatten/kanjihandler.cc \
    src/util/converter.cc \
    src/util/stringutils.cc \
    src/util/databaseutils.cc \
    src/search/kanjientry.cc \
    src/update/updater.cc \
    src/update/downloader.cc \
    src/search/wordsearch.cc \
    src/search/kanjisearch.cc \
    src/search/sentencesearch.cc \
    src/search/sentenceentry.cc \
    src/search/namesearch.cc \
    src/search/nameentry.cc \
    src/flatten/namehandler.cc \
    src/ui/mytabwidget.cc \
    src/search/engineeringentry.cc \
    src/search/engineeringsearch.cc

HEADERS  += src/ui/mainwindow.h \
    src/search/entry.h \
    src/search/wordentry.h \
    src/search/search.h \
    src/flatten/flattener.h \
    src/flatten/defaulthandlerfordb.h \
    src/flatten/wordhandler.h \
    src/flatten/kanjihandler.h \
    src/util/converter.h \
    src/util/stringutils.h \
    src/util/databaseutils.h \
    src/search/kanjientry.h \
    src/util/mode.h \
    src/update/updater.h \
    src/update/downloader.h \
    src/search/wordsearch.h \
    src/search/kanjisearch.h \
    src/search/sentencesearch.h \
    src/search/sentenceentry.h \
    src/search/namesearch.h \
    src/search/nameentry.h \
    src/flatten/namehandler.h \
    src/ui/mytabwidget.h \
    src/search/engineeringentry.h \
    src/search/engineeringsearch.h

RESOURCES += kantanjisho.qrc

QMAKE_CXXFLAGS += -Wall
QMAKE_CXXFLAGS += -pedantic
QMAKE_CFLAGS += -DSQLITE_THREADSAFE=0

DEFINES += QT_NO_SSL
