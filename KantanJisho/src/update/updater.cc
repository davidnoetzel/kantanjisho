/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/update/updater.h"

#include <QDebug>
#include <QEventLoop>
#include <QFileInfo>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QtZlib/zlib.h>

const QString Updater::baseUrl = QLatin1String("http://ftp.monash.edu.au/pub/nihongo/");

Updater::Updater(QObject *parent) : QObject(parent)
{
    QStringList filenameList = QStringList()
                 << Flattener::wordsFilename
                 << Flattener::engineeringFilename
                 << Flattener::namesFilename
                 << Flattener::kanjiFilename
                 << Flattener::sentencesFilename;

    for (int i = 0; i < MODE_MAX; i++) {
        filenames.insert((Mode)i, filenameList.at(i));
        urls.insert((Mode)i, QUrl(baseUrl+filenameList.at(i)+".gz"));
    }

    downloader = new Downloader(this, false);
    connect(downloader, &Downloader::downloadProgress,
            this, &Updater::downloadProgress);

    thread = new QThread();
    flattener = new Flattener();
    flattener->moveToThread(thread);
    thread->start();
    connect(this, &Updater::flattening, flattener, &Flattener::flatten);
    connect(flattener, &Flattener::flatteningDone, this, &Updater::updateFinished);
}

Updater::~Updater()
{
    QSettings settings("KantanJisho.ini", QSettings::IniFormat);
    settings.setValue("lastupdate", QDateTime::currentDateTime());
    thread->exit();
    delete downloader;
    delete thread;
    delete flattener;
}

QList<Mode> Updater::checkForUpdates()
{
    QList<Mode> updateRequired;
    QSettings settings("KantanJisho.ini", QSettings::IniFormat);

    for (int i = 0; i < MODE_MAX; i++) {
        QDateTime lastupdate = settings.value("lastupdate").toDateTime();

        if (isUpdateRequired((Mode)i, lastupdate)) {
            updateRequired.append((Mode)i);
        }
    }

    return updateRequired;
}

bool Updater::isUpdateRequired(Mode mode, QDateTime lastupdate)
{
    QUrl url = urls.value(mode);

    if (url.isEmpty()) {
        qWarning() << QString("isUpdateRequired(): URL for mode: %1 not found")
                      .arg(mode);
        return false;
    }

    QNetworkAccessManager manager;
    QNetworkReply *reply = manager.head(QNetworkRequest(url));
    QEventLoop loop;
    connect(reply, &QNetworkReply::finished,
            &loop, &QEventLoop::quit);
    loop.exec();
    QDateTime lastModfied = reply->header(QNetworkRequest::LastModifiedHeader)
                            .toDateTime();

    return (lastModfied >= lastupdate);
}

bool Updater::updateDictionary(Mode mode)
{
    QString filename = filenames.value(mode);

    if (filename.isEmpty()) {
        return false;
    }

    QUrl url = QUrl(baseUrl+filename+".gz");
    downloader->download(url);
    QEventLoop loop;
    connect(downloader, &Downloader::downloadFinished,
            &loop, &QEventLoop::quit);
    loop.exec();

    QByteArray contents = downloader->getContents();
    QTime t;
    t.start();
    contents = gzipUncompress(contents);
    qDebug("Decompress: %d ms", t.elapsed());
    flattener->setMode(mode);
    QFile file(flattener->getCompletePath());

    if (!file.open(QFile::WriteOnly)) {
        qWarning() << "updateDictionary(): Could not open file "
                   << file.fileName();
        return false;
    }

    file.write(contents);
    file.close();

    emit flattening(true);

    return true;
}

QByteArray Updater::gzipUncompress(const QByteArray &data)
{
    if (data.size() <= 4) {
        qWarning() << "gUncompress: Input data is truncated";
        return QByteArray();
    }

    QByteArray result;

    int ret;
    z_stream strm;
    static const int CHUNK_SIZE = 1024;
    char out[CHUNK_SIZE];

    // Allocate inflate state
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = data.size();
    strm.next_in = (Bytef*)(data.data());
    ret = inflateInit2(&strm, 15 +  32); // gzip decoding

    if (ret != Z_OK) {
        return QByteArray();
    }

    // Run inflate()
    do {
        strm.avail_out = CHUNK_SIZE;
        strm.next_out = (Bytef*)(out);

        ret = inflate(&strm, Z_NO_FLUSH);
        Q_ASSERT(ret != Z_STREAM_ERROR);  // State not clobbered

        switch (ret) {
        case Z_NEED_DICT:
            ret = Z_DATA_ERROR; // Fall through
        case Z_DATA_ERROR:
        case Z_MEM_ERROR:
            (void)inflateEnd(&strm);
            return QByteArray();
        }

        result.append(out, CHUNK_SIZE - strm.avail_out);
    } while (strm.avail_out == 0);

    // Clean up
    inflateEnd(&strm);

    return result;
}
