#include "src/update/downloader.h"

#include <QDebug>
#include <QEventLoop>
#include <QFile>
#include <QFileInfo>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

Downloader::Downloader(QObject *parent, bool writeToFile) : QObject(parent)
{
    this->writeToFile = writeToFile;
    manager = new QNetworkAccessManager(this);

    connect(manager, &QNetworkAccessManager::finished,
            this, &Downloader::processDownload);
}

Downloader::~Downloader()
{
    delete manager;
}

void Downloader::download(const QUrl &url)
{
    this->url = url;
    QNetworkRequest req(url);
    req.setRawHeader("Accept-Encoding", "gzip");
    QNetworkReply *reply = manager->get(req);
    connect(reply, &QNetworkReply::downloadProgress,
            this, &Downloader::downloadProgress);
    emit available(false);
    emit running(true);
}

void Downloader::processDownload(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError) {
        emit errorString(reply->errorString());
    }
    else {
        contents = reply->readAll();

        if (writeToFile) {
            QFileInfo fileInfo = url.path();
            QFile file(fileInfo.fileName());

            if (!file.open(QIODevice::WriteOnly)) {
                qWarning() << "Could not open file " << file.fileName();
                return;
            }

            file.write(contents);
            file.close();
            contents = QByteArray();
        }
    }

    reply->deleteLater();
    emit available(true);
    emit running(false);
    emit downloadFinished(true);
}

QByteArray Downloader::getContents()
{
    return contents;
}

