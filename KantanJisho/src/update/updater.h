/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KANTANJISHO_UPDATE_UPDATER_H
#define KANTANJISHO_UPDATE_UPDATER_H

#include <QMap>
#include <QObject>

#include "src/flatten/flattener.h"
#include "src/update/downloader.h"
#include "src/util/mode.h"

class Updater : public QObject
{
    Q_OBJECT

public:
    explicit Updater(QObject *parent = 0);
    virtual ~Updater();
    QList<Mode> checkForUpdates();
    bool isUpdateRequired(Mode mode, QDateTime lastupdate);
    bool updateDictionary(Mode mode);

signals:
    void downloadProgress(qint64, qint64);
    void flattening(bool);
    void updateFinished(bool);

private:
    QByteArray gzipUncompress(const QByteArray &data);
    static QMap<Mode, QString> initializeFilenameMap();
    static QMap<Mode, QUrl> initializeUrlMap();
    const static QString baseUrl;
    QMap<Mode, QString> filenames;
    QMap<Mode, QUrl> urls;
    Downloader *downloader;
    QThread *thread;
    Flattener *flattener;
};

#endif // KANTANJISHO_UPDATE_UPDATER_H
