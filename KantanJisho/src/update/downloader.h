#ifndef KANTANJISHO_UPDATE_DOWNLOADER_H
#define KANTANJISHO_UPDATE_DOWNLOADER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

class Downloader : public QObject
{
    Q_OBJECT

public:
    explicit Downloader(QObject *parent = 0, bool writeToFile = true);
    ~Downloader();
    QByteArray getContents();

public slots:
    void download(const QUrl &url);
    void processDownload(QNetworkReply *reply);

signals:
    void errorString(const QString &);
    void available(bool);
    void running(bool);
    void downloadFinished(bool);
    void downloadProgress(qint64, qint64);

private:
    QByteArray contents;
    bool writeToFile;
    QNetworkAccessManager *manager;
    QUrl url;
};

#endif // KANTANJISHO_UPDATE_DOWNLOADER_H
