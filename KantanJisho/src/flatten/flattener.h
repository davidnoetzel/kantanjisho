/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KANTANJISHO_FLATTEN_FLATTENER_H
#define KANTANJISHO_FLATTEN_FLATTENER_H

#include <QIODevice>
#include <QMap>
#include <QObject>
#include <QStandardPaths>
#include <QTextStream>
#include <QtSql>

#include "src/util/mode.h"

class Flattener : public QObject
{
    Q_OBJECT

public:
    /**
     * Constructs a new object to flatten a dictionary file.
     * @param mode The mode in which the file should be processed
     */
    Flattener(QObject *parent = 0);
    ~Flattener();
    void setMode(Mode mode);
    QString getCompletePath();
    /**
     * Customizes entities in the dictionary file or deletes problematic comments in the kanji file.
     */
    void preprocessXML();
    /**
     * Changes entities to be more concise and cleaner looking.
     * @param br The input stream
     * @param bw The output stream
     * @throws IOException If the file somehow cannot be read or written to
     */
    void customizeEntities(QTextStream &readStream, QTextStream &writeStream);
    /**
     * Removes comments which contains kanji that can cause XML parsing to fail.
     * @param br The input stream
     * @param bw The output stream
     * @throws IOException If the file somehow cannot be read or written to
     */
    void removeComments(QTextStream &readStream, QTextStream &writeStream);
    const static QString wordsFilename;
    const static QString engineeringFilename;
    const static QString namesFilename;
    const static QString kanjiFilename;
    const static QString sentencesFilename;

signals:
    void flatteningDone(bool);

public slots:
    /**
     * Writes the contents of various files to a database for faster searching.
     */
    void flatten();

private:
    void sentencesFlatten();
    void engineeringFlatten();
    /**
     * Writes the contents of an XML file to a database for faster searching.
     */
    void xmlFlatten();

    /**
     * Defines the entities in the JMdict XML file to be replaced.
     */
    static QMap<QString, QString> initializeWordEntities();
    /**
     * Defines the entities in the JMnedict XML file to be replaced.
     */
    static QMap<QString, QString> initializeNameEntities();
    /**
     * Defines the categories in the engscidic that need to be translated from a
     * number to a string.
     */
    static QMap<QString, QString> initializeEngineeringMarkers();

    /**
     * The XML file to be flattened.
     */
    QString filename;
    /**
     * The database table to which to write the data.
     */
    QString tableName;
    /**
     * The mode in which the file should be processed.
     */
    Mode mode;
    const static QMap<QString, QString> wordEntities;
    const static QMap<QString, QString> nameEntities;
    const static QMap<QString, QString> engineeringMarkers;
    QString saveDir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QString completePath;
    const static QString wordsTableName;
    const static QString engineeringTableName;
    const static QString namesTableName;
    const static QString kanjiTableName;
    const static QString sentencesTableName;
};

#endif // KANTANJISHO_FLATTEN_FLATTENER_H
