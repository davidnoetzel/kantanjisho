/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/flatten/flattener.h"

#include <QDebug>
#include <QFile>
#include <QStringBuilder>

#include "src/flatten/kanjihandler.h"
#include "src/flatten/namehandler.h"
#include "src/flatten/wordhandler.h"
#include "src/util/converter.h"
#include "src/util/databaseutils.h"
#include "src/util/stringutils.h"

const QString Flattener::wordsTableName = QLatin1String("words");
const QString Flattener::engineeringTableName = QLatin1String("engineering");
const QString Flattener::namesTableName = QLatin1String("names");
const QString Flattener::kanjiTableName = QLatin1String("kanji");
const QString Flattener::sentencesTableName = QLatin1String("sentences");
const QString Flattener::wordsFilename = QLatin1String("JMdict_e");
const QString Flattener::engineeringFilename = QLatin1String("engscidic");
const QString Flattener::namesFilename = QLatin1String("JMnedict.xml");
const QString Flattener::kanjiFilename = QLatin1String("kanjidic2.xml");
const QString Flattener::sentencesFilename = QLatin1String("examples.utf");
const QMap<QString, QString> Flattener::wordEntities = initializeWordEntities();
const QMap<QString, QString> Flattener::nameEntities = initializeNameEntities();
const QMap<QString, QString> Flattener::engineeringMarkers =
        initializeEngineeringMarkers();

/**
 * Constructs a new object to flatten XML files.
 * @param mode The mode in which XML files should be processed
 */
Flattener::Flattener(QObject *parent) : QObject(parent)
{

}

Flattener::~Flattener()
{

}

void Flattener::setMode(Mode mode)
{
    this->mode = mode;

    if (mode == Mode::WORDS) {
        filename = wordsFilename;
        tableName = wordsTableName;
    }
    else if (mode == Mode::ENGINEERING) {
        filename = engineeringFilename;
        tableName = engineeringTableName;
    }
    else if (mode == Mode::NAMES) {
        filename = namesFilename;
        tableName = namesTableName;
    }
    else if (mode == Mode::KANJI) {
        filename = kanjiFilename;
        tableName = kanjiTableName;
    }
    else if (mode == Mode::SENTENCES) {
        filename = sentencesFilename;
        tableName = sentencesTableName;
    }

    completePath = saveDir + "/" + filename;
}

QString Flattener::getCompletePath()
{
    return completePath;
}

/**
 * Writes the contents of various files to a database for faster searching.
 */
void Flattener::flatten()
{
    // Engineering and sentences dictionaries are not XML files, but rather a
    // custom format that needs to be handled differently
    if (mode == Mode::ENGINEERING) {
        engineeringFlatten();
    }
    else if (mode == Mode::SENTENCES) {
        sentencesFlatten();
    }
    else {
        preprocessXML();
        xmlFlatten();
    }

    emit flatteningDone(true);
}

void Flattener::engineeringFlatten()
{
    QFile file(completePath);

    if (!file.open(QFile::ReadOnly)) {
        qWarning() << "engineeringFlatten(): Could not open file "
                   << file.fileName();

        return;
    }

    int engineeringCols = 5;
    QList<QVariantList> data;

    for (int i = 0; i < engineeringCols; i++) {
        data.append(QVariantList());
    }

    QTime timer;
    timer.start();
    QTextStream readStream(&file);
    readStream.setCodec("EUC-JP");
    QString line = readStream.readLine();

    while (!(line.isEmpty() && readStream.atEnd())) {
        int readingBegin = line.indexOf('[');
        int readingEnd = line.indexOf(']');
        QString kanji = substring(line, 0, readingBegin-1);
        QString kana = substring(line, readingBegin+1, readingEnd);
        int meaningsBegin = line.indexOf('/');
        int meaningsEnd = line.lastIndexOf(' ');
        QString meanings = substring(line, meaningsBegin+1, meaningsEnd);
        int markersBegin = line.lastIndexOf('(');
        int markersEnd = line.lastIndexOf(')');
        QString markersNumber = substring(line, markersBegin+1, markersEnd);
        QString markers = engineeringMarkers.value(markersNumber);

        if (kanji == kana || Converter::isKatakana(kanji)) {
            data[0].append("");
            data[1].append(kanji);
            data[2].append(markers);
            data[3].append(meanings);
            data[4].append((int)Converter::isKatakana(kanji));
        }
        else {
            data[0].append(kanji);
            data[1].append(kana);
            data[2].append(markers);
            data[3].append(meanings);
            data[4].append(0);
        }

        line = readStream.readLine();
    }

    qDebug() << QString("File parsing: %1 ms").arg(timer.elapsed());

    QSqlQuery query;
    query.exec("drop table if exists "+tableName);
    DatabaseUtils::createTable(mode, tableName);
    DatabaseUtils::populateDatabase(data, tableName, engineeringCols);
    file.close();
    file.remove();
}

void Flattener::sentencesFlatten()
{
    QFile file(completePath);

    if (!file.open(QFile::ReadOnly)) {
        qWarning() << "sentencesFlatten(): Could not open file "
                   << file.fileName();

        return;
    }

    int sentencesCols = 3;
    QMap <QString, bool> alreadySeen;
    QList<QVariantList> data;
    int startPoint = 3;

    for (int i = 0; i < sentencesCols; i++) {
        data.append(QVariantList());
    }

    QTime timer;
    timer.start();
    QTextStream readStream(&file);
    readStream.setCodec("UTF-8");
    QString line = readStream.readLine();

    while (!(line.isEmpty() && readStream.atEnd())) {
        // Line with example sentences
        if (line.startsWith("A: ")) {
            int hashPoint = line.lastIndexOf('#');
            int tabPoint = line.lastIndexOf('\t');
            QString id = substring(line, hashPoint+4); //Omits #ID=
            QString japanese = substring(line, startPoint, tabPoint);
            QString english = substring(line, tabPoint+1, hashPoint);

            // The file can contain duplicates
            if (!alreadySeen.contains(id)) {
                data[0].append(id);
                data[1].append(japanese);
                data[2].append(english);
                alreadySeen.insert(id, true);
            }
        }

        line = readStream.readLine();
    }

    qDebug() << QString("File parsing: %1 ms").arg(timer.elapsed());

    QSqlQuery query;
    query.exec("drop table if exists "+tableName);
    DatabaseUtils::createTable(mode, tableName);
    DatabaseUtils::populateDatabase(data, tableName, sentencesCols);
    file.close();
    file.remove();
}

/**
 * Writes the contents of an XML file to a database for faster searching.
 */
void Flattener::xmlFlatten()
{
    DefaultHandlerForDB *handler;

    if (mode == Mode::WORDS) {
        handler = new WordHandler();
    }
    else if (mode == Mode::NAMES) {
        handler = new NameHandler();
    }
    else if (mode == Mode::KANJI) {
        handler = new KanjiHandler();
    }
    else {
        return;
    }

    QXmlSimpleReader reader;
    reader.setContentHandler(handler);
    reader.setErrorHandler(handler);

    QFile file(completePath);

    if (!file.open(QFile::ReadOnly)) {
        qWarning() << QString("xmlFlatten(): Could not open file: %1")
                      .arg(completePath);
        return;
    }

    QXmlInputSource xmlInputSource(&file);
    QTime timer;
    timer.start();

    if (reader.parse(xmlInputSource)) {
        qDebug() << QString("XML parsing: %1 ms").arg(timer.elapsed());
        QList<QVariantList> data = handler->getData();

        QSqlQuery query;
        query.exec("drop table if exists "+tableName);
        DatabaseUtils::createTable(mode, tableName);
        DatabaseUtils::populateDatabase(data, tableName, handler->getNumCols());
    }

    file.close();
    file.remove();
    delete handler;
}

/**
 * Defines the entities in the JMdict XML file to be replaced.
 */
QMap<QString, QString> Flattener::initializeWordEntities()
{
    QMap<QString, QString> m;

    m.insert("rude or X-rated term (not displayed in educational software)",
                     "rude or X-rated term");
    m.insert("adjective (keiyoushi)", "adjective");
    m.insert("adjectival nouns or quasi-adjectives (keiyodoshi)",
             "na-adjective");
    m.insert("nouns which may take the genitive case particle 'no'",
             "no-adjective");
    m.insert("pre-noun adjectival (rentaishi)", "pre-noun adjective");
    m.insert("adverb (fukushi)", "adverb");
    m.insert("Expressions (phrases, clauses, etc.)", "expression");
    m.insert("interjection (kandoushi)", "interjection");
    m.insert("noun (common) (futsuumeishi)", "noun");
    m.insert("adverbial noun (fukushitekimeishi)", "adverbial noun");
    m.insert("noun, used as a suffix", "noun suffix");
    m.insert("noun, used as a prefix", "noun prefix");
    m.insert("noun (temporal) (jisoumeishi)", "temporal noun");
    m.insert("noun or participle which takes the aux. verb suru", "suru verb");

    return m;
}

/**
 * Defines the entities in the JMnedict XML file to be replaced.
 */
QMap<QString, QString> Flattener::initializeNameEntities()
{
    QMap<QString, QString> m;

    m.insert("work of art, literature, music, etc. name",
                    "name of an artistic work");
    m.insert("male given name or forename", "male given name");
    m.insert("female given name or forename", "female given name");
    m.insert("given name or forename, gender not specified",
                    "given name (gender not specified)");

    return m;
}

/**
 * Defines the categories in the engscidic that need to be translated from a
 * number to a string.
 */
QMap<QString, QString> Flattener::initializeEngineeringMarkers()
{
    QMap<QString, QString> m;

    m.insert( "1", "計算力学");
    m.insert( "2", "バイオエンジニアリング");
    m.insert( "3", "環境工学");
    m.insert( "4", "産業・化学機械");
    m.insert( "5", "宇宙工学");
    m.insert( "6", "技術と社会");
    m.insert( "7", "材料力学");
    m.insert( "8", "機械材料・材料加工");
    m.insert( "9", "流体工学・流体機械");
    m.insert("10", "熱工学");
    m.insert("11", "エンジンシステム");
    m.insert("12", "動力エネルギーシステム");
    m.insert("13", "機械力学・計測制御");
    m.insert("14", "ロボティクス・メカトロニクス");
    m.insert("15", "情報・知能・精密機器");
    m.insert("16", "機素潤滑設計");
    m.insert("17", "設計工学・システム");
    m.insert("18", "生産加工・工作機械");
    m.insert("19", "ＦＡ（ファクトリーオートメーション）");
    m.insert("20", "交通・物流");

    return m;
}

/**
 * Customizes entities in the dictionary file or deletes problematic comments in the kanji file.
 */
void Flattener::preprocessXML()
{
    QString localFilename = completePath;
    QString tempFilename = saveDir+"/tmp_"+filename;
    QFile read(localFilename);
    QFile write(tempFilename);

    if (read.open(QIODevice::ReadOnly) &&
        write.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        QTextStream readStream(&read);
        readStream.setCodec("UTF-8");
        QTextStream writeStream(&write);
        writeStream.setCodec("UTF-8");

        QTime timer;
        timer.start();
        if (mode == Mode::WORDS || mode == Mode::NAMES) {
            customizeEntities(readStream, writeStream);
        }
        else if (mode == Mode::KANJI) {
            removeComments(readStream, writeStream);
        }
        qDebug() << QString("XML preprocessing: %1 ms").arg(timer.elapsed());

        read.remove();
        write.rename(localFilename);
        write.close();
    }
}

/**
 * Changes entities to be more concise and cleaner looking.
 * @param br The input stream
 * @param bw The output stream
 */
void Flattener::customizeEntities(QTextStream &readStream, QTextStream &writeStream)
{
    bool entitiesDone = false;
    QMap<QString, QString> entities;

    if (mode == Mode::WORDS) {
        entities = wordEntities;
    }
    else if (mode == Mode::NAMES) {
        entities = nameEntities;
    }

    while (!entitiesDone) {
        QString line = readStream.readLine();

        if (line.startsWith("<!ENTITY ")) {
            if (mode == Mode::WORDS) {
                line = line.replace(", etc. ", " ");
                line = line.replace('`', '\'');
            }

            QStringList entityList = line.split('"');
            QString entity = entityList.at(1);
            QString substitute = entities.value(entity);

            //It is an entity to be replaced
            if (!substitute.isEmpty()) {
                line = entityList[0]%'"'%substitute%'"'%entityList[2];
            }
        }
        else if ((mode == Mode::WORDS && line.startsWith("<JMdict>")) ||
                 (mode == Mode::NAMES && line.startsWith("<JMnedict>"))) {
            entitiesDone = true;
        }

        writeStream << line << '\n';
    }

    while (!readStream.atEnd()) {
        writeStream << readStream.readLine() << '\n';
    }
}

/**
 * Removes comments which contains kanji that can cause XML parsing to fail.
 * @param br The input stream
 * @param bw The output stream
 * @throws IOException If the file somehow cannot be read or written to
 */
void Flattener::removeComments(QTextStream &readStream, QTextStream &writeStream)
{
    QString line = readStream.readLine();

    while (!(line.isEmpty() && readStream.atEnd())) {
        //Some comments have Unicode characters that cause SAX errors
        if (!(line.startsWith("<!--") && line.endsWith("-->"))) {
            writeStream << line << '\n';
        }

        line = readStream.readLine();
    }
}
