/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KANTANJISHO_FLATTEN_WORDHANDLER_H
#define KANTANJISHO_FLATTEN_WORDHANDLER_H

#include "src/flatten/defaulthandlerfordb.h"

class WordHandler : public DefaultHandlerForDB
{
public:
    WordHandler();
    ~WordHandler();
    bool startElement(const QString &namespaceURI, const QString &localName,
                      const QString &qName, const QXmlAttributes &/* attributes */);
    bool endElement(const QString &namespaceURI, const QString &localName,
                    const QString &qName);
    bool characters(const QString &str);
    bool fatalError(const QXmlParseException &exception);
    QString errorString() const;
    /**
     * Returns a string of the markers ready to be displayed.
     * @return A semicolon separated string of word markers
     */
    QString formatMarkerString();
    /**
     * Returns a string of the definitions ready to be displayed.
     * @return An enumerated string of definitions
     */
    QString formatDefString();

    int numCols = 5;

private:
    QString currentText;
    QString errorStr;
    bool bent_seq = false;
    bool bk_ele = false;
    bool br_ele = false;
    bool bpos = false;
    bool bsense = false;
    bool bgloss = false;
    bool bre_restr = false;
    QString ent_seq;
    QStringList kanjiList;
    QList<QStringList> kanaList;
    QList<QStringList> meanings;
    QStringList markers;
};

#endif // KANTANJISHO_FLATTEN_WORDHANDLER_H
