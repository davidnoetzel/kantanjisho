/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KANTANJISHO_FLATTEN_KANJIHANDLER_H
#define KANTANJISHO_FLATTEN_KANJIHANDLER_H

#include "src/flatten/defaulthandlerfordb.h"

class KanjiHandler : public DefaultHandlerForDB
{
public:
    KanjiHandler();
    ~KanjiHandler();
    bool startElement(const QString &namespaceURI, const QString &localName,
                      const QString &qName, const QXmlAttributes &attributes);
    bool endElement(const QString &namespaceURI, const QString &localName,
                    const QString &qName);
    bool characters(const QString &str);
    bool fatalError(const QXmlParseException &exception);
    QString errorString() const;

private:
    QString currentText;
    QString errorStr;
    bool bliteral = false;
    bool brad_value = false;
    bool bgrade = false;
    bool bstroke_count = false;
    bool bfreq = false;
    bool bjlpt = false;
    bool breading = false;
    bool bon = false;
    bool bkun = false;
    bool bmeaning = false;
    bool bnanori = false;
    QString literal;
    QStringList radValues;
    QString grade;
    QString strokeCount;
    QString freq;
    QString jlpt;
    QStringList onYomi;
    QStringList kunYomi;
    QStringList meanings;
    QStringList nanori;
};

#endif // KANTANJISHO_FLATTEN_KANJIHANDLER_H
