/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/flatten/namehandler.h"

NameHandler::NameHandler()
{
    setNumCols(5);
    intializeData();
}

NameHandler::~NameHandler()
{

}

bool NameHandler::startElement(const QString & /* namespaceURI */,
                               const QString & /* localName */,
                               const QString &qName,
                               const QXmlAttributes & /* attributes */)
{
    currentText.clear();

    if (qName == QLatin1String("ent_seq")) {
        bent_seq = true;
    }
    else if (qName == QLatin1String("k_ele")) {
        bk_ele = true;
    }
    else if (qName == QLatin1String("r_ele")) {
        br_ele = true;
    }
    else if (qName == QLatin1String("name_type")) {
        bname_type = true;
    }
    else if (qName == QLatin1String("trans_det")) {
        btrans_det = true;
    }
    else if (qName == QLatin1String("re_restr")) {
        bre_restr = true;
    }

    return true;
}

bool NameHandler::endElement(const QString & /* namespaceURI */,
                             const QString & /* localName */,
                             const QString &qName)
{
    if (qName == QLatin1String("entry")) {
        QString typeString = typeList.join(", ");
        QString transcriptionString = transcriptionList.join(", ");

        QList<QStringList> entries;

        for (int i = 0; i < kanaList.size(); i++) {
            // Kana-only name
            if (kanjiList.size() == 0) {
                QStringList entry = QStringList();
                entry << QLatin1String("") << kanaList.at(i).at(0)
                      << typeString << transcriptionString;
                entries.append(entry);
                continue;
            }

            for (int j = 0; j < kanjiList.size(); j++) {
                QStringList kana = kanaList.at(i);
                QString restriction = kana.at(1);
                QString kanji = kanjiList.at(j);

                if (restriction.isNull() || restriction == kanji) {
                    QStringList entry;
                    entry << kanji << kana.at(0) << typeString
                          << transcriptionString;
                    entries.append(entry);
                }
            }
        }

        for (int i = 0; i < entries.size(); i++) {
            data[0].append(ent_seq.toInt());
            data[1].append(entries.at(i).at(0)); // Kanji
            data[2].append(entries.at(i).at(1)); // Kana
            data[3].append(entries.at(i).at(2)); // Type
            data[4].append(entries.at(i).at(3)); // Transcription
        }

        kanjiList.clear();
        kanaList.clear();
        typeList.clear();
        transcriptionList.clear();
    }
    if (bent_seq) {
        ent_seq = currentText;
        bent_seq = false;
    }
    if (bk_ele) {
        QString foundKanji = currentText;
        kanjiList.append(foundKanji);
        bk_ele = false;
    }
    if (br_ele) {
        QString foundKana = currentText;
        QStringList kanaPair;
        kanaPair << foundKana << QString();
        kanaList.append(kanaPair);
        br_ele = false;
    }
    if (bname_type) {
        QString foundType = currentText;
        //Capitalize first letter of type explanation
        foundType[0] = foundType[0].toUpper();
        typeList.append(foundType);
        bname_type = false;
    }
    if (btrans_det) {
        QString foundTranscription = currentText;
        transcriptionList.append(foundTranscription);
        btrans_det = false;
    }
    if (bre_restr) {
        QString foundRestriction = currentText;
        kanaList[kanaList.size()-1][1] = foundRestriction;
        bre_restr = false;
    }

    return true;
}

bool NameHandler::characters(const QString &str)
{
    currentText += str;
    return true;
}

bool NameHandler::fatalError(const QXmlParseException &exception)
{
    qWarning() << "Fatal error on line" << exception.lineNumber()
               << ", column" << exception.columnNumber() << ":"
               << exception.message();

    return false;
}

QString NameHandler::errorString() const
{
    return errorStr;
}
