/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/flatten/kanjihandler.h"

KanjiHandler::KanjiHandler()
{
    setNumCols(10);
    intializeData();
}

KanjiHandler::~KanjiHandler()
{

}

bool KanjiHandler::startElement(const QString & /* namespaceURI */,
                                const QString & /* localName */,
                                const QString &qName,
                                const QXmlAttributes &attributes)
{
    currentText.clear();

    if (qName == QLatin1String("literal")) {
        bliteral = true;
    }
    if (qName == QLatin1String("rad_value")) {
        brad_value = true;
    }
    if (qName == QLatin1String("grade")) {
        bgrade = true;
    }
    if (qName == QLatin1String("stroke_count")) {
        bstroke_count = true;
    }
    if (qName == QLatin1String("freq")) {
        bfreq = true;
    }
    if (qName == QLatin1String("jlpt")) {
        bjlpt = true;
    }
    if (qName == QLatin1String("reading")) {
        QString attrib = attributes.value("r_type");

        //Only record Japanese pronunciations
        if (attrib == QLatin1String("ja_on")) {
            breading = true;
            bon = true;
        }
        else if (attrib == QLatin1String("ja_kun")) {
            breading = true;
            bkun = true;
        }
    }
    if (qName == QLatin1String("meaning")) {
        QString attrib = attributes.value(QLatin1String("m_lang"));

        //An English meaning has no m_lang attribute
        if (attrib.isEmpty()) {
            bmeaning = true;
        }
    }
    if (qName == QLatin1String("nanori")) {
        bnanori = true;
    }

    return true;
}

bool KanjiHandler::endElement(const QString & /* namespaceURI */,
                              const QString & /* localName */,
                              const QString &qName)
{
    if (qName == QLatin1String("character")) {
        QString radString = radValues.join(";");
        QString onYomiString = onYomi.join(";");
        QString kunYomiString = kunYomi.join(";");
        QString meaningString = meanings.join(";");
        QString nanoriString = nanori.join(";");

        data[0].append(literal);
        data[1].append(radString);
        data[2].append(grade);
        data[3].append(strokeCount);
        data[4].append(freq);
        data[5].append(jlpt);
        data[6].append(onYomiString);
        data[7].append(kunYomiString);
        data[8].append(meaningString);
        data[9].append(nanoriString);

        radValues.clear();
        onYomi.clear();
        kunYomi.clear();
        meanings.clear();
        nanori.clear();
    }
    if (bliteral) {
        literal = currentText;
        bliteral = false;
    }
    if (brad_value) {
        QString foundRadValue = currentText;
        radValues.append(foundRadValue);
        brad_value = false;
    }
    if (bgrade) {
        grade = currentText;
        bgrade = false;
    }
    if (bstroke_count) {
        strokeCount = currentText;
        bstroke_count = false;
    }
    if (bfreq) {
        freq = currentText;
        bfreq = false;
    }
    if (bjlpt) {
        jlpt = currentText;
        bjlpt = false;
    }
    if (breading) {
        QString foundReading = currentText;

        if (bon) {
            onYomi.append(foundReading);
            bon = false;
        }
        else if (bkun) {
            kunYomi.append(foundReading);
            bkun = false;
        }

        breading = false;
    }
    if (bmeaning) {
        QString foundMeaning = currentText;
        meanings.append(foundMeaning);
        bmeaning = false;
    }
    if (bnanori) {
        QString foundNanori = currentText;
        nanori.append(foundNanori);
        bnanori = false;
    }

    return true;
}

bool KanjiHandler::characters(const QString &str)
{
    currentText += str;
    return true;
}

bool KanjiHandler::fatalError(const QXmlParseException &exception)
{
    qWarning() << "Fatal error on line" << exception.lineNumber()
               << ", column" << exception.columnNumber() << ":"
               << exception.message();

    return false;
}

QString KanjiHandler::errorString() const
{
    return errorStr;
}
