/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/flatten/wordhandler.h"

#include "src/util/converter.h"

WordHandler::WordHandler()
{
    setNumCols(6);
    intializeData();
}

WordHandler::~WordHandler()
{

}

bool WordHandler::startElement(const QString & /* namespaceURI */,
                               const QString & /* localName */,
                               const QString &qName,
                               const QXmlAttributes & /* attributes */)
{
    currentText.clear();

    if (qName == QLatin1String("ent_seq")) {
        bent_seq = true;
    }
    else if (qName == QLatin1String("k_ele")) {
        bk_ele = true;
    }
    else if (qName == QLatin1String("r_ele")) {
        br_ele = true;
    }
    else if (qName == QLatin1String("pos")) {
        bpos = true;
    }
    else if (qName == QLatin1String("sense")) {
        bsense = true;
    }
    else if (qName == QLatin1String("gloss")) {
        bgloss = true;
    }
    else if (qName == QLatin1String("re_restr")) {
        bre_restr = true;
    }

    return true;
}

bool WordHandler::endElement(const QString & /* namespaceURI */,
                             const QString & /* localName */,
                             const QString &qName)
{
    if (qName == QLatin1String("entry")) {
        QString markerString = formatMarkerString();
        QString defString = formatDefString();

        QList<QStringList> entries = QList<QStringList>();

        for (int i = 0; i < kanaList.size(); i++) {
            //Kana-only word
            if (kanjiList.size() == 0) {
                QStringList entry = QStringList();
                entry << QLatin1String("") << kanaList.at(i).at(0) << markerString << defString;
                entries.append(entry);
                continue;
            }

            for (int j = 0; j < kanjiList.size(); j++) {
                QStringList kana = kanaList.at(i);
                QString restriction = kana.at(1);
                QString kanji = kanjiList.at(j);

                if (restriction.isNull() || restriction == kanji) {
                    QStringList entry = QStringList();
                    entry << kanji << kana.at(0) << markerString << defString;
                    entries.append(entry);
                }
            }
        }

        for (int i = 0; i < entries.size(); i++) {
            data[0].append(ent_seq.toInt());
            data[1].append(entries.at(i).at(0)); // Kanji
            data[2].append(entries.at(i).at(1)); // Kana
            data[3].append(entries.at(i).at(2)); // Markers
            data[4].append(entries.at(i).at(3)); // Meanings
            // Katakana only
            data[5].append((int)Converter::isKatakana(entries.at(i).at(1)));
        }

        kanjiList.clear();
        kanaList.clear();
        meanings.clear();
        markers.clear();
    }
    if (bent_seq) {
        ent_seq = currentText;
        bent_seq = false;
    }
    if (bk_ele) {
        QString foundKanji = currentText;
        kanjiList.append(foundKanji);
        bk_ele = false;
    }
    if (br_ele) {
        QString foundKana = currentText;
        QStringList kanaPair = QStringList();
        kanaPair << foundKana << QString();
        kanaList.append(kanaPair);
        br_ele = false;
    }
    if (bpos) {
        QString foundMarker = currentText;
        //Capitalize first letter of marker explanation
        foundMarker[0] = foundMarker[0].toUpper();
        markers.append(foundMarker);
        bpos = false;
    }
    if (bsense) {
        meanings.append(QStringList());
        bsense = false;
    }
    if (bgloss) {
        QString foundMeaning = currentText;
        meanings[meanings.size()-1].append(foundMeaning);
        bgloss = false;
    }
    if (bre_restr) {
        QString foundRestriction = currentText;
        kanaList[kanaList.size()-1][1] = foundRestriction;
        bre_restr = false;
    }

    return true;
}

/**
 * Returns a string of the markers ready to be displayed.
 * @return A semicolon separated string of word markers
 */
QString WordHandler::formatMarkerString()
{
    return markers.join("; ");
}

/**
 * Returns a string of the definitions ready to be displayed.
 * @return An enumerated string of definitions
 */
QString WordHandler::formatDefString()
{
    QString defString = QLatin1String("");

    for (int i = 0; i < meanings.size(); i++) {
        defString += QString("%1. %2\n").arg(i+1).arg(meanings.at(i).join("; "));
    }

    return defString.trimmed();
}

bool WordHandler::characters(const QString &str)
{
    currentText += str;
    return true;
}

bool WordHandler::fatalError(const QXmlParseException &exception)
{
    qWarning() << "Fatal error on line" << exception.lineNumber()
               << ", column" << exception.columnNumber() << ":"
               << exception.message();

    return false;
}

QString WordHandler::errorString() const
{
    return errorStr;
}
