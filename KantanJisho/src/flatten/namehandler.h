/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KANTANJISHO_FLATTEN_NAMEHANDLER_H
#define KANTANJISHO_FLATTEN_NAMEHANDLER_H

#include "src/flatten/defaulthandlerfordb.h"

class NameHandler : public DefaultHandlerForDB
{
public:
    NameHandler();
    ~NameHandler();
    bool startElement(const QString &namespaceURI, const QString &localName,
                      const QString &qName, const QXmlAttributes &/* attributes */);
    bool endElement(const QString &namespaceURI, const QString &localName,
                    const QString &qName);
    bool characters(const QString &str);
    bool fatalError(const QXmlParseException &exception);
    QString errorString() const;

    int numCols = 5;

private:
    QString currentText;
    QString errorStr;
    bool bent_seq = false;
    bool bk_ele = false;
    bool br_ele = false;
    bool btrans = false;
    bool bname_type = false;
    bool btrans_det = false;
    bool bre_restr = false;
    QString ent_seq;
    QStringList kanjiList;
    QList<QStringList> kanaList;
    QStringList typeList;
    QStringList transcriptionList;
};

#endif // KANTANJISHO_FLATTEN_NAMEHANDLER_H
