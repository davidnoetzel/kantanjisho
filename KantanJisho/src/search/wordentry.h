/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KANTANJISHO_SEARCH_WORDENTRY_H
#define KANTANJISHO_SEARCH_WORDENTRY_H

#include "src/search/entry.h"

#include <QStringList>

class WordEntry : public virtual Entry
{
public:
    WordEntry();
    WordEntry(QString kana, QString markers, QString meanings) :
        WordEntry(QString(""), kana, markers, meanings) {}
    WordEntry(QString kanji, QString kana, QString markers, QString meanings);
    WordEntry(QStringList line) :
        WordEntry(line.at(0), line.at(1), line.at(2), line.at(3)) {}
    ~WordEntry();
    QString toString();
    int getAverageLength();

private:
    QString kanji;
    QString kana;
    QString markers;
    QString meanings;
    const static int averageLength = 75;
};

#endif // KANTANJISHO_SEARCH_WORDENTRY_H
