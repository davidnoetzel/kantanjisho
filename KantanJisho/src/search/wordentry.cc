/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/search/wordentry.h"

WordEntry::WordEntry(QString kanji, QString kana, QString markers, QString meanings)
{
    this->kanji = kanji;
    this->kana = kana;
    this->markers = markers;
    this->meanings = meanings;
}

WordEntry::~WordEntry()
{

}

QString WordEntry::toString()
{
    if (kanji.isEmpty()) {
        return QString("%1\n%2\n%3").arg(kana).arg(markers).arg(meanings);
    }
    else {
        return QString("%1\n%2\n%3\n%4").arg(kanji).arg(kana).arg(markers).arg(meanings);
    }
}

int WordEntry::getAverageLength()
{
    return averageLength;
}
