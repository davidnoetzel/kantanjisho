/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KANTANJISHO_SEARCH_NAMESEARCH_H
#define KANTANJISHO_SEARCH_NAMESEARCH_H

#include "src/search/search.h"

class NameSearch : public Search
{
public:
    NameSearch(QString jTerm, QString eTerm, bool exact);
    ~NameSearch();
    void storeResults(QSqlQuery query);
    QSqlQuery generateQuery(QString regex);
};

#endif // KANTANJISHO_SEARCH_NAMESEARCH_H
