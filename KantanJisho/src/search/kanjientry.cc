/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "kanjientry.h"

KanjiEntry::KanjiEntry(QString kanji,
                       QString radValues,
                       QString grade,
                       QString strokeCount,
                       QString frequency,
                       QString jlptLevel,
                       QString onYomi,
                       QString kunYomi,
                       QString meanings,
                       QString nanori)
{
    this->kanji = kanji;
    this->radValues = radValues;
    this->grade = grade;
    this->strokeCount = strokeCount;
    this->frequency = frequency;
    this->jlptLevel = jlptLevel;
    this->onYomi = onYomi;
    this->kunYomi = kunYomi;
    this->meanings = meanings;
    this->nanori = nanori;
}

KanjiEntry::~KanjiEntry()
{

}

QString KanjiEntry::toString()
{
    return QString("%1\n%2\n%3\n%4\n%5").arg(kanji)
                                        .arg(onYomi.replace(";", ", "))
                                        .arg(kunYomi.replace(";", ", "))
                                        .arg(nanori.replace(";", ", "))
                                        .arg(meanings);
}

int KanjiEntry::getAverageLength()
{
    return averageLength;
}
