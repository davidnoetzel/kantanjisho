/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/search/wordsearch.h"

#include "src/search/wordentry.h"

WordSearch::WordSearch(QString jTerm, QString eTerm, bool exact, bool hideKatakana)
    : Search(jTerm, eTerm, exact)
{
    this->hideKatakana = hideKatakana;
    tabPrefix = "G";
}

WordSearch::~WordSearch()
{

}

QSqlQuery WordSearch::generateQuery(QString regex)
{
    QString select = "select kanji, kana, markers, meanings, katakana_only "
                     "from ";
    QString table = "words ";

    return generateGenericQuery(regex, select, table, hideKatakana);
}

void WordSearch::storeResults(QSqlQuery query)
{
    while (query.next()) {
        QString kanji = query.value(0).toString();
        QString kana = query.value(1).toString();
        QString markers = query.value(2).toString();
        QString meanings = query.value(3).toString();

        results.append(new WordEntry(kanji, kana, markers, meanings));
    }
}
