/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/search/namesearch.h"

#include "src/search/nameentry.h"

NameSearch::NameSearch(QString jTerm, QString eTerm, bool exact)
    : Search(jTerm, eTerm, exact)
{
    tabPrefix = "N";
}

NameSearch::~NameSearch()
{

}

QSqlQuery NameSearch::generateQuery(QString regex)
{
    QString select = "select kanji, kana, types, transcriptions from ";
    QString table = "names ";

    return generateGenericQuery(regex, select, table, false);
}

void NameSearch::storeResults(QSqlQuery query)
{
    while (query.next()) {
        QString kanji = query.value(0).toString();
        QString kana = query.value(1).toString();
        QString types = query.value(2).toString();
        QString transcriptions = query.value(3).toString();

        results.append(new NameEntry(kanji, kana, types, transcriptions));
    }
}
