/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/search/search.h"

#include <QDebug>
#include <QRegularExpression>
#include <QStringBuilder>
#include <QtSql>

#include "src/search/kanjientry.h"
#include "src/search/wordentry.h"

Search::Search(QString jTerm, QString eTerm, bool exact)
{
    this->jTerm = jTerm.trimmed().toLower();
    this->eTerm = eTerm.trimmed().toLower();
    this->exact = exact;
    err = NoError;
}

Search::~Search()
{
    qDeleteAll(results);
    results.clear();
}

bool Search::search()
{
    if (jTerm.isEmpty() && eTerm.isEmpty()) {
        return true;
    }

    isKana = false;
    isKanji = false;

    if (!jTerm.isEmpty()) {
        if (Converter::isRoman(jTerm)) {
            jTerm = Converter::convertRomajiToHiragana(jTerm);

            if (jTerm.isEmpty()) {
                err = RomanizationError;
                return false;
            }

            isKana = true;
        }
        else {
            isKana = Converter::isKana(jTerm);
            isKanji = !isKana;
        }

        if (exact) {
            jTerm = jTerm.endsWith('.') ? jTerm : jTerm.append('.');
        }
    }

    QString regex = Converter::convertToRegex(jTerm);
    QSqlQuery query = generateQuery(regex);
    query.exec();
    storeResults(query);

    if (hasResults()) {
        saveToHistory();
    }

    return true;
}

QSqlQuery Search::generateGenericQuery(QString regex, QString select,
                                       QString table, bool hideKatakana)
{
    QString queryString = select;
    QSqlQuery query;
    bool hasCondition = false;
    QStringList tokens;
    bool isBlob = regex.indexOf('*') > 0 &&
                  regex.indexOf('*') != regex.size()-1;

    if (isKana) {
        hasCondition = true;
        tokens = Converter::tokenizeKana(regex);

        for (int i = 0; i < tokens.size()-1; i++) {
            queryString += "(" % select;
        }

        queryString += table;

        for (int i = 0; i < tokens.size(); i++) {
            if (Converter::hasRegex(tokens.at(i))) {
                queryString += "where kana glob ? ";
            }
            else {
                queryString += "where kana glob ? or kana glob ? ";
            }

            if (i < tokens.size()-1) {
                queryString += ") ";
            }
        }
    }
    else if (isKanji) {
        hasCondition = true;

        for (int i = 0; i < regex.size()-1; i++) {
            queryString += "(" % select;
        }

        queryString += table;

        for (int i = 0; i < regex.size(); i++) {
            queryString += "where kanji glob ? ";

            if (i < regex.size()-1) {
                queryString += ") ";
            }
        }
    }
    else {
        queryString += table;
    }

    if (!eTerm.isEmpty()) {
        if (!hasCondition) {
            queryString += "where ";
            hasCondition = true;
        }
        else {
            queryString += "and ";
        }

        // The names table searches English terms against a column called
        // transcriptions; words and engineering have a column called meaning
        queryString += tabPrefix == QLatin1String("N") ?
                       "transcriptions like ? " : "meanings like ? ";
    }

    if (hideKatakana) {
        queryString += "and katakana_only = 0 ";
    }

    query.prepare(queryString);

    if (isKana) {
        for (int i = 0; i < tokens.size(); i++) {
            QString bindValueBase;
            QString bindValue1;
            QString bindValue2;

            for (int j = 0; j < i; j++) {
                if (tokens.at(j) == "*") {
                    bindValueBase += "*";
                }
                else if (tokens.at(j).size() == 2) {
                    bindValueBase += "??";
                }
                else {
                    bindValueBase += "?";
                }
            }

            if (Converter::hasRegex(tokens.at(i))) {
                bindValue1 = bindValueBase % tokens.at(i);
                bindValue2 = bindValueBase % tokens.at(i);
            }
            else if (Converter::isHiragana(tokens.at(i))) {
                 bindValue1 = bindValueBase % tokens.at(i);
                 bindValue2 = bindValueBase %
                             Converter::convertHiraganaToKatakana(tokens.at(i));
            }
            else if (Converter::isKatakana(tokens.at(i))) {
                 bindValue1 = bindValueBase %
                             Converter::convertKatakanaToHiragana(tokens.at(i));
                 bindValue2 = bindValueBase % tokens.at(i);
            }

            if (isBlob) {
                for (int j = i+1; j < tokens.size(); j++) {
                    QString additionalBindValue;
                    // Add question marks for each kana, not just each token
                    // Keep in mind edge cases like ko*ko*sei
                    if (Converter::isKana(tokens.at(j)) && tokens.at(j).size() == 2) {
                        additionalBindValue = "??";
                    }
                    else if (tokens.at(j) == QLatin1String("*")) {
                        additionalBindValue = tokens.at(j);
                    }
                    else {
                        additionalBindValue = "?";
                    }

                    bindValue1 += additionalBindValue;
                    bindValue2 += additionalBindValue;
                }
            }
            else if (i < tokens.size()-1) {
                bindValue1 += '*';
                bindValue2 += '*';
            }

            query.addBindValue(bindValue1);

            if (bindValue1 != bindValue2) {
                query.addBindValue(bindValue2);
            }
        }
    }
    else if (isKanji) {
        for (int i = 0; i < regex.size(); i++) {
            QString bindValue;

            for (int j = 0; j < i; j++) {
                bindValue += regex.at(j);
            }

            bindValue += regex.at(i);

            if (i < regex.size()-1) {
                bindValue += '*';
            }

            query.addBindValue(bindValue);
        }
    }

    if (!eTerm.isEmpty()) {
        query.addBindValue('%'+eTerm+'%');
    }

    return query;
}

bool Search::hasResults()
{
    return !results.isEmpty();
}

void Search::saveToHistory()
{
    QString queryString = "insert into history values (?, ?, ?)";
    QSqlQuery query;
    query.prepare(queryString);
    query.addBindValue(QDateTime::currentMSecsSinceEpoch());
    query.addBindValue(jTerm);
    query.addBindValue(eTerm);

    if (!query.exec()) {
        qWarning() << query.lastError();
    }
}

QString Search::tabTitle()
{
    if (err != NoError) {
        return "Error";
    }

    if (!jTerm.isEmpty() && !eTerm.isEmpty()) {
        return tabPrefix+": "+jTerm+"/"+eTerm;
    }
    else if (!jTerm.isEmpty()) {
        return tabPrefix+": "+jTerm;
    }
    else {
        return tabPrefix+": "+eTerm;
    }
}

QString Search::tabContents()
{
    if (err == RomanizationError) {
        return "Error while converting romaji. Are you sure you "
               "entered your search term correctly?";
    }
    else if (results.isEmpty()) {
        return "No results";
    }

    QString returnString;
    returnString.reserve(results.at(0)->getAverageLength());

    for (int i = 0; i < results.size(); i++) {
        returnString += QString("%1\n\n").arg(results.at(i)->toString());
    }

    returnString.squeeze();

    return returnString;
}

Search::SearchError Search::error()
{
    return err;
}

