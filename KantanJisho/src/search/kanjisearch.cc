/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/search/kanjisearch.h"

#include "src/search/kanjientry.h"

KanjiSearch::KanjiSearch(QString jTerm, QString eTerm, bool exact)
    : Search(jTerm, eTerm, exact)
{
    tabPrefix = "K";
}

KanjiSearch::~KanjiSearch()
{

}

QSqlQuery KanjiSearch::generateQuery(QString regex)
{
    QString select = "select * from kanji ";
    QString queryString = select;
    QSqlQuery query;
    bool hasCondition = false;

    if (isKana && Converter::isHiragana(regex)) {
        queryString += "where kunYomi glob ? "
                       "or nanori glob ? ";
    }
    else if (isKana && Converter::isKatakana(regex)) {
        queryString += "where onYomi glob ? ";
    }
    else if (isKanji) {
        queryString += "where kanji glob ? ";
    }

    if (!eTerm.isEmpty()) {
        if (!hasCondition) {
            queryString += "where ";
            hasCondition = true;
        }
        else {
            queryString += "and ";
        }

        queryString += "meanings like ? ";
    }

    query.prepare(queryString);

    if (isKana && Converter::isHiragana(regex)) {
        query.addBindValue("*"+regex);
        query.addBindValue("*"+regex);
    }
    else if (isKana && Converter::isKatakana(regex)) {
        query.addBindValue("*"+regex);
    }
    else if (isKanji) {
        query.addBindValue(regex);
    }

    if (!eTerm.isEmpty()) {
        query.addBindValue('%'+eTerm+'%');
    }

    return query;
}

void KanjiSearch::storeResults(QSqlQuery query)
{
    while (query.next()) {
        QString kanji = query.value(0).toString();
        QString radValues = query.value(1).toString();
        QString grade = query.value(2).toString();
        QString strokeCount = query.value(3).toString();
        QString frequency = query.value(4).toString();
        QString jlptLevel = query.value(5).toString();
        QString onYomi = query.value(6).toString();
        QString kunYomi = query.value(7).toString();
        QString meanings = query.value(8).toString();
        QString nanori = query.value(9).toString();

        results.append(new KanjiEntry(kanji, radValues, grade, strokeCount,
                                      frequency, jlptLevel, onYomi, kunYomi,
                                      meanings, nanori));
    }
}
