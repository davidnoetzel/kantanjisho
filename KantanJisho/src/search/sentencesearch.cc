/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/search/sentencesearch.h"

#include "src/search/sentenceentry.h"

SentenceSearch::SentenceSearch(QString jTerm, QString eTerm)
    : Search(jTerm, eTerm, false)
{
    tabPrefix = "S";
}

SentenceSearch::~SentenceSearch()
{

}

QSqlQuery SentenceSearch::generateQuery(QString regex)
{
    QString queryString = "select japanese, english from sentences ";
    QSqlQuery query;
    bool hasCondition = false;

    if (!regex.isEmpty()) {
        hasCondition = true;
        queryString += "where japanese glob ? ";
    }
    if (!eTerm.isEmpty()) {
        if (!hasCondition) {
            queryString += "where ";
            hasCondition = true;
        }
        else {
            queryString += "and ";
        }

        queryString += "english like ? ";
    }

    query.prepare(queryString);

    if (!regex.isEmpty()) {
        query.addBindValue('*'+regex);
    }
    if (!eTerm.isEmpty()) {
        query.addBindValue('%'+eTerm+'%');
    }

    return query;
}

void SentenceSearch::storeResults(QSqlQuery query)
{
    while (query.next()) {
        QString japanese = query.value(0).toString();
        QString english = query.value(1).toString();

        results.append(new SentenceEntry(japanese, english));
    }
}
