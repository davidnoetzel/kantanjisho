/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KANTANJISHO_SEARCH_SEARCH_H
#define KANTANJISHO_SEARCH_SEARCH_H

#include <QList>
#include <QtSql>

#include "src/search/entry.h"
#include "src/util/converter.h"
#include "src/util/mode.h"

class Search
{
public:
    enum SearchError { NoError = 0,
                       InvalidDictionaryError = 1,
                       RomanizationError = 2
                     };

    Search(QString jTerm, QString eTerm, bool exact);
    virtual ~Search();
    bool search();
    virtual QSqlQuery generateQuery(QString regex) = 0;
    QSqlQuery generateGenericQuery(QString regex, QString select,
                                   QString table, bool hideKatakana);
    virtual void storeResults(QSqlQuery query) = 0;
    bool hasResults();
    void saveToHistory();
    QString tabTitle();
    QString tabContents();
    SearchError error();

protected:
    QList<Entry*> results;
    bool exact;
    QString jTerm;
    QString eTerm;
    bool isKanji;
    bool isKana;
    QString tabPrefix;
    SearchError err;
};

#endif // KANTANJISHO_SEARCH_SEARCH_H
