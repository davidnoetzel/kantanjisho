/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/ui/mytabwidget.h"

#include <QMouseEvent>
#include <QTabBar>

MyTabWidget::MyTabWidget(QWidget *parent)
    : QTabWidget(parent)
{
    tabBar()->installEventFilter(this);
}

MyTabWidget::~MyTabWidget()
{

}

bool MyTabWidget::eventFilter(QObject *o, QEvent *e)
{
    if (o == tabBar() && e->type() == QEvent::MouseButtonPress) {
        QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(e);

        if (mouseEvent->buttons() == Qt::MidButton) {
            emit tabCloseRequested(tabBar()->tabAt(mouseEvent->pos()));

            return true;
        }
        else {
            return QTabWidget::eventFilter(o, e);
        }
    }

    return QTabWidget::eventFilter(o, e);
}
