/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KANTANJISHO_UI_MAINWINDOW_H
#define KANTANJISHO_UI_MAINWINDOW_H

#include <QCheckBox>
#include <QComboBox>
#include <QLineEdit>
#include <QMainWindow>
#include <QProgressBar>
#include <QPushButton>
#include <QTextEdit>

#include "src/ui/mytabwidget.h"
#include "src/util/mode.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow();

private slots:
    void firstRunCheck();
    void autoCheckForUpdates();
    void downloadProgress(qint64 r, qint64 t);
    void flattening();
    void search();
    void reset();
    void newBlankTab();
    void newTab(QString title, QString body);
    void closeCurrentTab();
    void closeTab(int index);
    void viewHistory();
    void clearHistory();
    void cut();
    void copy();
    void paste();
    void del();
    void selectAll();
    void checkForUpdates();
    void update(QList<Mode> updatesRequired);
    void about();

private:
    void createActions();
    void createMenus();
    void setSelectable(Mode mode, bool setting);
    void setSelectable(int i, bool setting);
    bool isSelectable(Mode mode);
    bool isSelectable(int i);
    bool isSearchBox(QWidget *focused);

    QLineEdit *jSearch;
    QLineEdit *eSearch;
    QPushButton *searchButton;
    QPushButton *resetButton;
    QCheckBox *exactOnlyBox;
    QCheckBox *hideKatakanaBox;
    QComboBox *dictSelect;
    QProgressBar *progressBar;
    MyTabWidget *tabWidget;
    QAction *newTabAct;
    QAction *closeTabAct;
    QAction *viewHistoryAct;
    QAction *clearHistoryAct;
    QAction *exitAct;
    QAction *cutAct;
    QAction *copyAct;
    QAction *pasteAct;
    QAction *delAct;
    QAction *selectAllAct;
    QAction *updateAct;
    QAction *aboutAct;
    QAction *aboutQtAct;
    QList<QTextEdit*> tabContents;
};

#endif // KANTANJISHO_UI_MAINWINDOW_H
