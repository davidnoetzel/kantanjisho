/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KANTANJISHO_UI_MYTABWIDGET_H
#define KANTANJISHO_UI_MYTABWIDGET_H

#include <QTabWidget>

class MyTabWidget : public QTabWidget
{
public:
    MyTabWidget(QWidget *parent = 0);
    ~MyTabWidget();
    bool eventFilter(QObject *o, QEvent *e);
};

#endif // KANTANJISHO_UI_MYTABWIDGET_H
