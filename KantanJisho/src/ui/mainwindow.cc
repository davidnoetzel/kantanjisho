/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/ui/mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QFile>
#include <QGridLayout>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QResource>
#include <QSettings>
#include <QStandardItem>
#include <QTime>
#include <QTimer>
#include <QVBoxLayout>

#include "src/search/engineeringsearch.h"
#include "src/search/kanjisearch.h"
#include "src/search/namesearch.h"
#include "src/search/search.h"
#include "src/search/sentencesearch.h"
#include "src/search/wordsearch.h"
#include "src/update/updater.h"
#include "src/util/databaseutils.h"

MainWindow::MainWindow()
{
    setWindowTitle(tr("Kantan Jisho"));
    setUnifiedTitleAndToolBarOnMac(true);

    QWidget *center = new QWidget(this);
    QVBoxLayout *mainLayout = new QVBoxLayout();
    QGridLayout *topFrame = new QGridLayout();
    QLabel *jLabel = new QLabel(tr("Japanese"));
    QLabel *eLabel = new QLabel(tr("English"));
    jSearch = new QLineEdit();
    eSearch = new QLineEdit();
    searchButton = new QPushButton(tr("Search"));
    resetButton = new QPushButton(tr("Reset"));
    QString jOnlyTooltip = "Only applies to the Japanese search term";
    exactOnlyBox = new QCheckBox(tr("Exact Matches Only"));
    exactOnlyBox->setToolTip(tr("Return exact matches only\n%1").arg(jOnlyTooltip));
    hideKatakanaBox = new QCheckBox(tr("Hide Katakana Words"));
    hideKatakanaBox->setToolTip(tr("Hide results that are only composed of "
                                   "katakana"));
    dictSelect = new QComboBox();
    dictSelect->addItem(tr("General dictionary (JMdict)"));
    dictSelect->addItem(tr("Engineering terms (engscidic)"));
    dictSelect->addItem(tr("Names and places (JMnedict)"));
    dictSelect->addItem(tr("Kanji (kanjidic)"));
    dictSelect->addItem(tr("Sentences"));
    progressBar = new QProgressBar(this);
    progressBar->setMinimum(0);
    progressBar->setMaximum(0);
    progressBar->setValue(0);
    progressBar->setAlignment(Qt::AlignCenter);
    progressBar->setVisible(false);
    tabWidget = new MyTabWidget();
    tabWidget->setTabsClosable(true);
    tabWidget->setMovable(true);

    topFrame->addWidget(jLabel, 0, 0, 1, 1, Qt::AlignRight);
    topFrame->addWidget(eLabel, 1, 0, 1, 1, Qt::AlignRight);
    topFrame->addWidget(jSearch, 0, 1, 1, 1);
    topFrame->addWidget(eSearch, 1, 1, 1, 1);
    topFrame->addWidget(searchButton, 0, 2, 1, 1);
    topFrame->addWidget(resetButton, 1, 2, 1, 1);
    topFrame->addWidget(exactOnlyBox, 0, 3, 1, 1);
    topFrame->addWidget(hideKatakanaBox, 1, 3, 1, 1);
    topFrame->addWidget(dictSelect, 0, 4, 1, 1);
    topFrame->addWidget(progressBar, 1, 4, 1, 1);

    mainLayout->addLayout(topFrame);
    mainLayout->addWidget(tabWidget);
    center->setLayout(mainLayout);
    setCentralWidget(center);
    resize(800, 600);

    createActions();
    createMenus();
    DatabaseUtils::initializeDatabase();
    DatabaseUtils::openConnection();

    QTimer::singleShot(2000, this, &MainWindow::firstRunCheck);
}

void MainWindow::firstRunCheck()
{
    QSettings settings("KantanJisho.ini", QSettings::IniFormat);
    bool firstrun = settings.value("firstrun").toBool();

    if (!firstrun) {
        QTimer::singleShot(1000, this, &MainWindow::autoCheckForUpdates);

        return;
    }

    DatabaseUtils::createHistoryTable();

    for (int i = 0; i < MODE_MAX; i++) {
        setSelectable(i, false);
    }

    QMessageBox firstRunPrompt;
    firstRunPrompt.setText(tr("This appears to be your first time running "
                              "Kantan Jisho"));
    const char body[] = "To use this program, dictionary files need to be "
                        "downloaded. The files will begin downloading in "
                        "the background and will become selectable in the "
                        "drop-down menu once they are ready.";
    firstRunPrompt.setInformativeText(tr(body));
    firstRunPrompt.setStandardButtons(QMessageBox::Ok);
    firstRunPrompt.setDefaultButton(QMessageBox::Ok);
    firstRunPrompt.exec();
    QList<Mode> updatesRequired;

    for (int i = 0; i < MODE_MAX; i++) {
        updatesRequired.append((Mode)i);
    }

    update(updatesRequired);
}

void MainWindow::autoCheckForUpdates()
{
    QSettings settings("KantanJisho.ini", QSettings::IniFormat);
    QDateTime lastupdate = settings.value("lastupdate").toDateTime();
    qint64 updateinterval = settings.value("updateinterval").toLongLong();

    if (QDateTime::currentMSecsSinceEpoch() - lastupdate.toMSecsSinceEpoch() >=
        updateinterval) {
        checkForUpdates();
    }
}

void MainWindow::setSelectable(Mode mode, bool setting)
{
    setSelectable((int)mode, setting);
}

void MainWindow::setSelectable(int i, bool setting)
{
    QStandardItemModel *model =
            qobject_cast<QStandardItemModel*>(dictSelect->model());
    QModelIndex index = model->index(i, dictSelect->modelColumn(),
                                     dictSelect->rootModelIndex());
    QStandardItem *item = model->itemFromIndex(index);
    item->setSelectable(setting);

    if (setting) {
        item->setText(item->text().replace(" (Disabled)", ""));
    }
    else {
        item->setText(item->text()+" (Disabled)");
    }
}

bool MainWindow::isSelectable(Mode mode)
{
    return isSelectable((int)mode);
}

bool MainWindow::isSelectable(int i)
{
    QStandardItemModel *model =
            qobject_cast<QStandardItemModel*>(dictSelect->model());
    QModelIndex index = model->index(i, dictSelect->modelColumn(),
                                     dictSelect->rootModelIndex());
    QStandardItem *item = model->itemFromIndex(index);

    return item->isSelectable();
}

void MainWindow::search()
{
    if (!isSelectable(dictSelect->currentIndex())) {
        newTab("Dictionary disabled",
               "This dictionary is being updated. Please wait until it has "
               "finished updating to use it.");

        return;
    }

    QString jTerm = jSearch->text();
    QString eTerm = eSearch->text();
    bool exact = exactOnlyBox->isChecked();
    bool hideKatakana = hideKatakanaBox->isChecked();
    Mode mode = (Mode)dictSelect->currentIndex();
    Search *search;

    if (mode == Mode::WORDS) {
        search = new WordSearch(jTerm, eTerm, exact, hideKatakana);
    }
    else if (mode == Mode::ENGINEERING) {
        search = new EngineeringSearch(jTerm, eTerm, exact, hideKatakana);
    }
    else if (mode == Mode::NAMES) {
        search = new NameSearch(jTerm, eTerm, exact);
    }
    else if (mode == Mode::KANJI) {
        search = new KanjiSearch(jTerm, eTerm, exact);
    }
    else if (mode == Mode::SENTENCES) {
        search = new SentenceSearch(jTerm, eTerm);
    }

    QTime t;
    t.start();
    search->search();
    qDebug("Search: %d ms", t.elapsed());
    t.start();
    newTab(search->tabTitle(), search->tabContents());
    qDebug("Tab construction: %d ms", t.elapsed());
    delete search;
}

void MainWindow::reset()
{
    jSearch->setText("");
    eSearch->setText("");
    jSearch->setFocus();
}

void MainWindow::newBlankTab()
{
    newTab("New tab", "");
}

void MainWindow::newTab(QString title, QString body)
{
    QTextEdit *field = new QTextEdit();
    field->setReadOnly(true);
    field->setText(body);
    field->setFont(QFont("Georgia", 12));
    tabWidget->setCurrentIndex(tabWidget->addTab(field, title));
    tabContents.append(field);
}

void MainWindow::closeCurrentTab()
{
    int index = tabWidget->currentIndex();
    closeTab(index);
}

void MainWindow::closeTab(int index)
{
    if (index >= 0) {
        tabWidget->removeTab(index);
        QTextEdit *field = tabContents[index];
        field->clear();
        delete field;
        tabContents.removeAt(index);
    }
}

void MainWindow::viewHistory()
{
    QString body = DatabaseUtils::getHistory();
    newTab("History", body);
}

void MainWindow::clearHistory()
{
    DatabaseUtils::createHistoryTable();
}

bool MainWindow::isSearchBox(QWidget *focused)
{
    return focused == jSearch || focused == eSearch;
}

void MainWindow::cut()
{
    QWidget *focused = QApplication::focusWidget();

    if (isSearchBox(focused)) {
        QLineEdit *search = (QLineEdit*)focused;
        search->cut();
    }
}

void MainWindow::copy()
{
    QWidget *focused = QApplication::focusWidget();
    QTextEdit *currentField = (QTextEdit*)tabWidget->currentWidget();

    if (focused == currentField) {
        currentField->copy();
    }
    else if (isSearchBox(focused)) {
        QLineEdit *search = (QLineEdit*)focused;
        search->copy();
    }
}

void MainWindow::paste()
{
    QWidget *focused = QApplication::focusWidget();

    if (isSearchBox(focused)) {
        QLineEdit *search = (QLineEdit*)focused;
        search->paste();
    }
}

void MainWindow::del()
{
    QWidget *focused = QApplication::focusWidget();

    if (isSearchBox(focused)) {
        QLineEdit *search = (QLineEdit*)focused;

        if (search->hasSelectedText()) {
            search->del();
        }
    }
}

void MainWindow::selectAll()
{
    QWidget *focused = QApplication::focusWidget();
    QTextEdit *currentField = (QTextEdit*)tabWidget->currentWidget();

    if (focused == currentField) {
        currentField->selectAll();
    }
    else if (isSearchBox(focused)) {
        QLineEdit *search = (QLineEdit*)focused;
        search->selectAll();
    }
}

void MainWindow::checkForUpdates()
{
    Updater updater;
    QList<Mode> updatesRequired = updater.checkForUpdates();
    update(updatesRequired);
}

void MainWindow::update(QList<Mode> updatesRequired)
{
    for (int i = 0 ; i < updatesRequired.size(); i++) {
        if (isSelectable(updatesRequired.at(i))) {
            setSelectable(updatesRequired.at(i), false);
        }

        Updater updater(this);
        connect(&updater, &Updater::downloadProgress,
                this, &MainWindow::downloadProgress);
        connect(&updater, &Updater::flattening,
                this, &MainWindow::flattening);
        progressBar->setFormat(tr("Downloading file... %p%"));
        progressBar->setVisible(true);
        QEventLoop loop;
        connect(&updater, &Updater::updateFinished,
                &loop, &QEventLoop::quit);
        updater.updateDictionary(updatesRequired.at(i));
        loop.exec();
        progressBar->setVisible(false);
        setSelectable(updatesRequired.at(i), true);
    }
}

void MainWindow::about()
{
    QString title =  tr("About Kantan Jisho");
    QFile aboutFile(":/data/about.html");

    if (!aboutFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qWarning() << "Unable to open file: " << aboutFile.fileName() <<
                    " besause of error " << aboutFile.errorString() << endl;
        return;
    }

    QString description = tr(aboutFile.readAll());
    aboutFile.close();
    QMessageBox::about(this, title, description);
}

void MainWindow::createActions()
{
    connect(tabWidget, &QTabWidget::tabCloseRequested,
            this, &MainWindow::closeTab);

    connect(searchButton, &QPushButton::clicked, this, &MainWindow::search);

    connect(resetButton, &QPushButton::clicked, this, &MainWindow::reset);

    newTabAct = new QAction(tr("New &Tab"), this);
    newTabAct->setShortcuts(QKeySequence::AddTab);
    newTabAct->setStatusTip(tr("Create a new tab"));
    connect(newTabAct, &QAction::triggered, this, &MainWindow::newBlankTab);

    closeTabAct = new QAction(tr("&Close Tab"), this);
    closeTabAct->setShortcuts(QKeySequence::Close);
    closeTabAct->setStatusTip(tr("Close the current tab"));
    connect(closeTabAct, &QAction::triggered,
            this, &MainWindow::closeCurrentTab);

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, &QAction::triggered, this, &MainWindow::close);

    cutAct = new QAction(tr("Cu&t"), this);
    cutAct->setShortcuts(QKeySequence::Cut);
    cutAct->setStatusTip(tr("Cut the current selection's contents to the "
                            "clipboard"));
    connect(cutAct, &QAction::triggered, this, &MainWindow::cut);

    copyAct = new QAction(tr("&Copy"), this);
    copyAct->setShortcuts(QKeySequence::Copy);
    copyAct->setStatusTip(tr("Copy the current selection's contents to the "
                             "clipboard"));
    connect(copyAct, &QAction::triggered, this, &MainWindow::copy);

    pasteAct = new QAction(tr("&Paste"), this);
    pasteAct->setShortcuts(QKeySequence::Paste);
    pasteAct->setStatusTip(tr("Paste the clipboard's contents into the current "
                              "selection"));
    connect(pasteAct, &QAction::triggered, this, &MainWindow::paste);

    delAct = new QAction(tr("&Del"), this);
    delAct->setShortcuts(QKeySequence::Delete);
    delAct->setStatusTip(tr("Delete the current selection"));
    connect(delAct, &QAction::triggered, this, &MainWindow::del);

    selectAllAct = new QAction(tr("Select &All"), this);
    selectAllAct->setShortcuts(QKeySequence::SelectAll);
    selectAllAct->setStatusTip(tr("Select everything"));
    connect(selectAllAct, &QAction::triggered, this, &MainWindow::selectAll);

    viewHistoryAct = new QAction(tr("View &History"), this);
    viewHistoryAct->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_H));
    viewHistoryAct->setStatusTip(tr("View your search history"));
    connect(viewHistoryAct, &QAction::triggered,
            this, &MainWindow::viewHistory);

    clearHistoryAct = new QAction(tr("Clear History"), this);
    clearHistoryAct->setStatusTip(tr("Clear your search history"));
    connect(clearHistoryAct, &QAction::triggered,
            this, &MainWindow::clearHistory);

    updateAct = new QAction(tr("Check for &Updates"), this);
    updateAct->setStatusTip(tr("Check for updates to Kantan Jisho"));
    connect(updateAct, &QAction::triggered, this, &MainWindow::checkForUpdates);

    aboutAct = new QAction(tr("&About"), this);
    aboutAct->setStatusTip(tr("Show Kantan Jisho's About box"));
    connect(aboutAct, &QAction::triggered, this, &MainWindow::about);

    aboutQtAct = new QAction(tr("About &Qt"), this);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
    connect(aboutQtAct, &QAction::triggered, qApp, &QApplication::aboutQt);
}

void MainWindow::createMenus()
{
    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newTabAct);
    fileMenu->addAction(closeTabAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    QMenu *editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(cutAct);
    editMenu->addAction(copyAct);
    editMenu->addAction(pasteAct);
    editMenu->addSeparator();
    editMenu->addAction(delAct);
    editMenu->addSeparator();
    editMenu->addAction(selectAllAct);

    QMenu *historyMenu = menuBar()->addMenu(tr("Hi&story"));
    historyMenu->addAction(viewHistoryAct);
    historyMenu->addAction(clearHistoryAct);

    QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(updateAct);
    helpMenu->addSeparator();
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);
}

void MainWindow::downloadProgress(qint64 r, qint64 t)
{
    progressBar->setValue(r);
    progressBar->setMaximum(t);
}

void MainWindow::flattening()
{
    progressBar->setFormat("Processing file...");
    progressBar->setValue(100);
    progressBar->setMaximum(100);
}
