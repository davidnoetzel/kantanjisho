/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "databaseutils.h"

#include <QDebug>
#include <QtSql>
#include <QRegularExpression>
#include <QStringBuilder>

DatabaseUtils::DatabaseUtils()
{

}

DatabaseUtils::~DatabaseUtils()
{

}

void DatabaseUtils::initializeDatabase()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("kantanjisho.sqlite");
}

bool DatabaseUtils::openConnection()
{
    QSqlDatabase db = QSqlDatabase::database();

    if (!db.open()) {
        qWarning() << "Could not open database";
        return false;
    }

    return true;
}

void DatabaseUtils::createHistoryTable()
{
    QSqlQuery query;
    query.exec("drop table if exists history");
    QString queryString = "create table history ("
                          "    timestamp int,"
                          "    japanese varchar(100),"
                          "    english varchar(200),"
                          "    primary key(timestamp, japanese, english))";

    if (!query.exec(queryString)) {
        qWarning() << query.lastError();
    }
}

QString DatabaseUtils::getHistory()
{
    QSqlQuery query;
    query.exec("select * from history order by timestamp desc");
    QString results = "<html><head><style>"
                      "table, th, td { border-collapse: collapse; }"
                      "th, td { padding: 5px; }"
                      "</style></head><body><table>"
                      "<tr><th>Timestamp</th>"
                      "<th>Japanese</th>"
                      "<th>English</th></tr>";

    while (query.next()) {
        qlonglong timestamp = query.value(0).toLongLong();
        QString timeString = QDateTime::fromMSecsSinceEpoch(timestamp).toString();
        QString japanese = query.value(1).toString();
        QString english = query.value(2).toString();
        results += "<tr><td>" % timeString % "</td>";
        results += "<td>" % japanese % "</td>";
        results += "<td>" % english % "</td></tr>";
    }

    results += "</table></body></html>";

    return results;
}

void DatabaseUtils::createTable(Mode mode, QString tableName)
{
    QSqlQuery query;
    QString queryString;

    if (mode == Mode::WORDS) {
        queryString = "create table "+tableName+" ("
                      "    id int,"
                      "    kanji varchar(100),"
                      "    kana varchar(100),"
                      "    markers varchar(300),"
                      "    meanings varchar(2000),"
                      "    katakana_only int,"
                      "    primary key(id, kanji, kana))";
    }
    else if (mode == Mode::ENGINEERING) {
        queryString = "create table "+tableName+" ("
                      "    kanji varchar(50),"
                      "    kana varchar(60),"
                      "    markers varchar(20),"
                      "    meanings varchar(200),"
                      "    katakana_only int,"
                      "    primary key(kanji, kana, meanings))";
    }
    else if (mode == Mode::NAMES) {
        queryString = "create table "+tableName+" ("
                      "    id int,"
                      "    kanji varchar(50),"
                      "    kana varchar(100),"
                      "    types varchar(200),"
                      "    transcriptions varchar(500),"
                      "    primary key(id, kanji, kana))";
    }
    else if (mode == Mode::KANJI) {
        queryString = "create table "+tableName+" ("
                      "    kanji varchar(2),"
                      "    radValues varchar(15),"
                      "    grade int,"
                      "    strokeCount int,"
                      "    frequency int,"
                      "    jlptLevel int,"
                      "    onYomi varchar(50),"
                      "    kunYomi varchar(150),"
                      "    meanings varchar(300),"
                      "    nanori varchar(150),"
                      "    primary key(kanji))";
    }
    else if (mode == Mode::SENTENCES) {
        queryString = "create table "+tableName+" ("
                      "    id varchar(20),"
                      "    japanese varchar(400),"
                      "    english varchar(500), "
                      "    primary key(id))";
    }
    else {
        return;
    }

    if (!query.exec(queryString)) {
        qWarning() << query.lastError();
    }
}

void DatabaseUtils::populateDatabase(QList<QVariantList> data, QString tableName, int numCols)
{
    QSqlQuery query;
    QString queryString = generateQuery(tableName, numCols);
    query.prepare(queryString);

    for (int i = 0; i < numCols; i++) {
        query.addBindValue(data[i]);
    }

    QTime timer;
    timer.start();
    QSqlDatabase db = QSqlDatabase::database();
    db.transaction();
    bool succeeded = query.execBatch();
    db.commit();
    qDebug() << QString("DB insertion: %1 ms").arg(timer.elapsed());

    if (!succeeded) {
        qWarning() << query.lastError();
    }
}

QString DatabaseUtils::generateQuery(QString tableName, int numCols)
{
    QString queryString = QString("insert into %1 values(?").arg(tableName);

    for (int i = 0; i < numCols - 1; i++) {
        queryString += ", ?";
    }

    queryString += ')';

    return queryString;
}
