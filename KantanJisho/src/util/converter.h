/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KANTANJISHO_UTIL_CONVERTER_H
#define KANTANJISHO_UTIL_CONVERTER_H

#include <QMap>
#include <QObject>

class Converter
{
public:
    Converter();
    ~Converter();
    static bool isVowel(QChar c);
    static bool isModifier(QChar c);
    static QStringList tokenizeKana(QString jTerm);
    static QString convertToRegex(QString jTerm);
    static QString expandKana(QString jTerm);
    static QString convertRomajiToHiragana(QString jTerm);
    static QString convertKanaToRomaji(QString jTerm);
    static QString convertHiraganaToKatakana(QString jTerm);
    static QChar convertHiraganaToKatakana(QChar c);
    static QString convertKatakanaToHiragana(QString jTerm);
    static QChar convertKatakanaToHiragana(QChar c);
    static bool isRoman(QString jTerm);
    static bool isKana(QString jTerm);
    static bool isHiragana(QString jTerm);
    static bool isHiragana(QChar c);
    static bool isKatakana(QString jTerm);
    static bool isKatakana(QChar c);
    static bool isSpecialKana(QString jTerm);
    static bool isSpecialKana(QChar c);
    static bool hasRegex(QString jTerm);
    static bool isRegex(QChar c);

private:
    static QMap<QString, QString> initHiraganaMap();
    static QMap<QString, QString> initKatakanaMap();
    static QMap<QString, QString> initRomajiMap();

    const static QMap<QString, QString> hiraganaTable;
    const static QMap<QString, QString> katakanaTable;
    const static QMap<QString, QString> romajiTable;

    const static int ARRAY_LENGTH = 160;
    const static QString HIRAGANA_ARRAY[];
    const static QString KATAKANA_ARRAY[];
    const static QString ROMAJI_ARRAY[];
    const static int kanaDifference;
};

#endif // KANTANJISHO_UTIL_CONVERTER_H
