/*
 * Kantan Jisho - A simple, straightforward Japanese-multilingual dictionary
 *
 * Copyright (C) 2015  David Noetzel
 * This file is part of Kantan Jisho.
 *
 * Kantan Jisho is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kantan Jisho is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kantan Jisho.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "src/util/converter.h"

#include <QDebug>
#include <QRegularExpression>

#include "src/util/stringutils.h"

Converter::Converter()
{

}

Converter::~Converter()
{

}

//Still missing a bunch of obscure entries with special combinations
const QString Converter::HIRAGANA_ARRAY[] =
            {"あ", "い", "う", "え", "お",
             "か", "き", "く", "け", "こ",
             "が", "ぎ", "ぐ", "げ", "ご",
             "さ", "し", "す", "せ", "そ",
             "ざ", "じ", "ず", "ぜ", "ぞ",
             "た", "ち", "つ", "て", "と",
             "だ", "ぢ", "づ", "で", "ど",
             "な", "に", "ぬ", "ね", "の",
             "は", "ひ", "ふ", "へ", "ほ",
             "ば", "び", "ぶ", "べ", "ぼ",
             "ぱ", "ぴ", "ぷ", "ぺ", "ぽ",
             "ま", "み", "む", "め", "も",
             "や",      "ゆ",      "よ",
             "ら", "り", "る", "れ", "ろ",
             "わ", "ゐ",      "ゑ", "を",
             "ん",
             "きゃ", "きゅ", "きょ",
             "ぎゃ", "ぎゅ", "ぎょ",
             "しゃ", "しゅ", "しょ",
             "じゃ", "じゅ", "じょ",
             "ちゃ", "ちゅ", "ちょ",
             "にゃ", "にゅ", "にょ",
             "ひゃ", "ひゅ", "ひょ",
             "びゃ", "びゅ", "びょ",
             "ぴゃ", "ぴゅ", "ぴょ",
             "みゃ", "みゅ", "みょ",
             "りゃ", "りゅ", "りょ",
             "ぁ", "ぃ", "ぅ", "ぇ", "ぉ",
             "いぇ",
             "うぃ", "うぇ", "うぅ",
             "ヴぁ", "ヴぃ", "ヴぅ", "ヴぇ", "ヴぉ",
             "ヴゃ", "ヴゅ", "ヴょ",
             "くぁ", "くぃ", "くぇ", "くぉ",
             "ぐぁ", "ぐぃ", "ぐぇ", "ぐぉ",
             "しぇ", "じぇ", "じぇ", "ちぇ",
             "つぁ", "つぃ", "つぇ", "つぉ",
             "てゃ", "てゅ", "てょ",
             "ぢゃ", "ぢゅ", "ぢょ",
             "ふぁ", "ふぃ", "ふぇ", "ふぉ",
             "ふゃ", "ふゅ", "ふょ",
             "ぶゅ",
             "でゅ", "ドュ",
             "ー", "・", ".", "*", "?"};
const QString Converter::KATAKANA_ARRAY[] =
            {"ア", "イ", "ウ", "エ", "オ",
             "カ", "キ", "ク", "ケ", "コ",
             "ガ", "ギ", "グ", "ゲ", "ゴ",
             "サ", "シ", "ス", "セ", "ソ",
             "ザ", "ジ", "ズ", "ゼ", "ゾ",
             "タ", "チ", "ツ", "テ", "ト",
             "ダ", "ヂ", "ヅ", "デ", "ド",
             "ナ", "ニ", "ヌ", "ネ", "ノ",
             "ハ", "ヒ", "フ", "ヘ", "ホ",
             "バ", "ビ", "ブ", "ベ", "ボ",
             "パ", "ピ", "プ", "ペ", "ポ",
             "マ", "ミ", "ム", "メ", "モ",
             "ヤ",      "ユ",      "ヨ",
             "ラ", "リ", "ル", "レ", "ロ",
             "ワ", "ヰ",      "ヱ", "ヲ",
             "ン",
             "キャ", "キュ", "キョ",
             "ギャ", "ギュ", "ギョ",
             "シャ", "シュ", "ショ",
             "ジャ", "ジュ", "ジョ",
             "チャ", "チュ", "チョ",
             "ニャ", "ニュ", "ニョ",
             "ヒャ", "ヒュ", "ヒョ",
             "ビャ", "ビュ", "ビョ",
             "ピャ", "ピュ", "ピョ",
             "ミャ", "ミュ", "ミョ",
             "リャ", "リュ", "リョ",
             "ァ", "ィ", "ゥ", "ェ", "ォ",
             "イェ",
             "ウィ", "ウェ", "ウヮ",
             "ヴァ", "ヴィ", "ヴ", "ヴェ", "ヴォ",
             "ヴャ", "ヴュ", "ヴョ",
             "クァ", "クィ", "クェ", "クォ",
             "グァ", "グィ", "グェ", "グォ",
             "シェ", "ジェ", "ジェ", "チェ",
             "ツァ", "ツィ", "ツェ", "ツァ",
             "テャ", "テュ", "テョ",
             "ヂャ", "ヂュ", "ヂョ",
             "ファ", "フィ", "フェ", "フォ",
             "フャ", "フュ", "フョ",
             "ブュ",
             "デュ", "ドュ",
             "ー", "・", ".", "*", "?"};
const QString Converter::ROMAJI_ARRAY[] = //Uses Hepburn romanization
            {"a",  "i",  "u",  "e",  "o",
             "ka", "ki", "ku", "ke", "ko",
             "ga", "gi", "gu", "ge", "go",
             "sa", "shi", "su", "se", "so",
             "za", "ji", "zu", "ze", "zo",
             "ta", "chi", "tsu", "te", "to",
             "da", "di", "du", "de", "do",
             "na", "ni", "nu", "ne", "no",
             "ha", "hi", "hu", "he", "ho",
             "ba", "bi", "bu", "be", "bo",
             "pa", "pi", "pu", "pe", "po",
             "ma", "mi", "mu", "me", "mo",
             "ya",       "yu",       "yo",
             "ra", "ri", "ru", "re", "ro",
             "wa", "wi",       "we", "wo",
             "n",
             "kya", "kyu", "kyo",
             "gya", "gyu", "gyo",
             "sha", "shu", "sho",
             "ja",  "ju",  "jo",
             "cha", "chu", "cho",
             "nya", "nyu", "nyo",
             "hya", "hyu", "hyo",
             "bya", "byu", "byo",
             "pya", "pyu", "pyo",
             "mya", "myu", "myo",
             "rya", "ryu", "ryo",
             "xa", "xi", "xu", "xe", "xo",
             "ye",
             "wi", "we", "wu",
             "va", "vi", "vu", "ve", "vo",
             "vya", "vyu", "vyo",
             "kwa", "kwi", "kwe", "kwo",
             "gwa", "gwi", "gwe", "gwo",
             "she", "zhe", "je", "che",
             "tsa", "tsi", "tse", "tso",
             "tha", "thu", "tho",
             "dya", "dyu", "dyo",
             "fa", "fi", "fe", "fo",
             "fya", "fyu", "fyo",
             "byyu",
             "dyu", "dyyu",
             "-", "　", ".", "*", "?"};
const QMap<QString, QString> Converter::hiraganaTable = initHiraganaMap();
const QMap<QString, QString> Converter::katakanaTable = initKatakanaMap();
const QMap<QString, QString> Converter::romajiTable = initRomajiMap();
const int Converter::kanaDifference = QString("ァ").at(0).unicode() -
                                      QString("ぁ").at(0).unicode();

/**
 * Creates a map between romanized syllables and their hiragana equivalents.
 */
QMap<QString, QString> Converter::initRomajiMap()
{
    QMap<QString, QString> m;
    int altSize = 18;
    QString altRomaji[] = {"si", "zi", "ti", "tu", "dzu", "hu", "oh", "nn", "n'",
                           "sya", "syu", "syo", "tya", "tyu", "tyo",
                           "zya", "zyu", "zyo"};
    QString altHiragana[] = {"し", "じ", "ち", "つ", "づ", "ふ", "おう", "ん", "ん",
                             "しゃ", "しゅ", "しょ", "ちゃ", "ちゅ", "ちょ",
                             "じゃ", "じゅ", "じょ"};

    for (int i = 0; i < ARRAY_LENGTH; i++) {
        m.insert(ROMAJI_ARRAY[i], HIRAGANA_ARRAY[i]);
    }

    for (int i = 0; i < altSize; i++) {
        m.insert(altRomaji[i], altHiragana[i]);
    }

    return m;
}

/**
 * Creates a map between hiragana syllables and their romanized equivalents.
 */
QMap<QString, QString> Converter::initHiraganaMap()
{
    QMap<QString, QString> m;

    for (int i = 0; i < ARRAY_LENGTH; i++) {
        m.insert(HIRAGANA_ARRAY[i], ROMAJI_ARRAY[i]);
    }

    return m;
}

/**
 * Creates a map between katakana syllables and their romanized equivalents.
 */
QMap<QString, QString> Converter::initKatakanaMap()
{
    QMap<QString, QString> m;

    for (int i = 0; i < ARRAY_LENGTH; i++) {
        m.insert(KATAKANA_ARRAY[i], ROMAJI_ARRAY[i]);
    }

    return m;
}

/**
 * Determines whether a given character is an English vowel. Y is not
 * considered a vowel, since it is not used as such in romaji.
 * @param c The character to be evaluated
 */
bool Converter::isVowel(QChar c)
{
    if (c == 'a' || c == 'e'  || c == 'i' || c == 'o'  || c == 'u') {
        return true;
    }
    else {
        return false;
    }
}

/**
 * Determines whether a given character is one that not an independent
 * mora, but rather a character that modifies another sound.
 * @param c The character to be evaluated
 */
bool Converter::isModifier(QChar c)
{
    if (c == QString("ゃ") || c == QString("ょ") || c == QString("ゅ") ||
        c == QString("ャ") || c == QString("ョ") || c == QString("ュ")) {
        return true;
    }
    else {
        return false;
    }
}

/**
 * Breaks a kana word into its individual mora.
 * @param jTerm The kana to be tokenized
 * @return An ArrayList of the mora that comprise the word
 */
QStringList Converter::tokenizeKana(QString jTerm)
{
    QStringList tokenList = QStringList();

    if (isModifier(jTerm.at(0)) || jTerm.isEmpty()) {
        return tokenList;
    }

    for (int i = 0; i < jTerm.size(); i++) {
        //The mora is both the modifier and the char it is modifying
        if (isModifier(jTerm.at(i))) {
            tokenList.last() += jTerm.at(i);
        }
        else {
            tokenList.append(jTerm.at(i));
        }
    }

    return tokenList;
}

/**
 * Converts a simple wildcard expression into a SQL GLOB pattern.
 * @param jTerm The term to be converted
 * @return A regex pattern or the original term, if it had no wildcards
 */
QString Converter::convertToRegex(QString jTerm)
{
    QString regex;

    regex = jTerm.replace(QString("＊"), "*")
                 .replace(QString("？"), "?");

    //Explicitly do not search for more words
    if (regex.endsWith('.') || regex.endsWith(QString("。"))) {
        regex.chop(1);
    }
    //No regex; do a basic search which should match only the beginning characters
    else if (!hasRegex(jTerm)) {
        regex += "*";
    }

    return regex;
}

/**
 * Returns a string of kana with its associated paired syllables from another script.
 * @param jTerm The kana to be made into a regex
 * @return A regex that will match the given kana
 */
QString Converter::expandKana(QString jTerm)
{
    QString s;
    s.reserve(jTerm.size() * 4);

    for (int i = 0; i < jTerm.size(); i++) {
        QChar c = jTerm.at(i);

        if (isHiragana(c)) {
            s.append('[');
            s.append(c);
            s.append(convertHiraganaToKatakana(c));
            s.append(']');
        }
        else if (isKatakana(c)) {
            s.append('[');
            s.append(convertKatakanaToHiragana(c));
            s.append(c);
            s.append(']');
        }
        else {
            s.append(c);
        }
    }

    return s;
}

/**
 * Converts a romanized string to hiragana
 * @param jTerm A romaji string to be made into hiragana
 * @return A hiragana version of the romaji string
 */
QString Converter::convertRomajiToHiragana(QString jTerm)
{
    int i = 0;
    int j = 1;
    QString lookup;
    jTerm.reserve(jTerm.size());
    QString newJTerm;

    while (i < jTerm.size()) {
        bool dubs = false;
        lookup.append(jTerm.at(i));

        if (j <= jTerm.size()) {
            lookup = substring(jTerm, i, j);
        }
        else {
            return QString();
        }

        if (lookup == QLatin1String("n")) {
            //C = consonant, V = vowel
            if (j < jTerm.size()) {
                //nV or n' or nn
                if (isVowel(jTerm.at(j)) || jTerm.at(j) == '\'' ||
                    jTerm.at(j) == 'n') {
                    j++;
                    lookup = substring(jTerm, i, j);
                }
            }
            //nC or out of chars
        }

        //CCV or CCCV
        if ((lookup.size() == 3 || lookup.size() == 4) &&
            lookup.at(0) == lookup.at(1)) {
            dubs = true;
            lookup = lookup.mid(1);
        }

        QString result = romajiTable.value(lookup);

        if (result.isEmpty()) {
            j++;
        }
        else {
            if (dubs) {
                newJTerm += "っ";
                dubs = false;
            }

            newJTerm += result;
            i = j;
            j++;
        }
    }

    return newJTerm;
}

/**
 * Converts a kana string to romaji.
 * @param jTerm A hiragana or katakana string to be romanized
 * @return A romaji version of the kana string
 */
QString Converter::convertKanaToRomaji(QString jTerm)
{
    QMap<QString, QString> table;

    if (isHiragana(jTerm)) {
        table = hiraganaTable;
    }
    else if (isKatakana(jTerm)) {
        table = katakanaTable;
    }
    else {
        return "";
    }

    QStringList tokens = tokenizeKana(jTerm);

    bool dubs = false;
    QString romaji;

    for (int i = 0; i < tokens.size(); i++) {
        if (tokens.at(i) == QString("っ") || tokens.at(i) == QString("ッ")) {
            dubs = true;
        }
        else if (dubs == true) {
            dubs = false;
            QString newTerm = table.value(tokens.at(i));
            romaji.append(newTerm.at(0) + newTerm);
        }
        else {
            QString newTerm = table.value(tokens.at(i));
            romaji.append(newTerm);
        }
    }

    return romaji;
}

/**
 * Converts a hiragana string to katakana.
 * @param jTerm A hiragana string to be made into katakana
 * @return A katakana version of the hiragana string
 */
QString Converter::convertHiraganaToKatakana(QString jTerm)
{
    QString katakana;
    katakana.reserve(jTerm.length());

    for (int i = 0; i < jTerm.size(); i++) {
        QChar c = jTerm.at(i);

        if (!isHiragana(c)) {
            return QString();
        }

        katakana += convertHiraganaToKatakana(c);
    }

    return katakana;
}

/**
 * Converts a hiragana char to katakana.
 * @param jTerm A hiragana char to be made into katakana
 * @return A katakana version of the hiragana char
 */
QChar Converter::convertHiraganaToKatakana(QChar c)
{
    return QChar(c.unicode() + kanaDifference);
}

/**
 * Converts a katakana string to hiragana.
 * @param jTerm A katakana string to be made into hiragana
 * @return A hiragana version of the katakana string
 */
QString Converter::convertKatakanaToHiragana(QString jTerm)
{
    QString hiragana;
    hiragana.reserve(jTerm.length());

    for (int i = 0; i < jTerm.size(); i++) {
        QChar c = jTerm.at(i);

        if (!isKatakana(c)) {
            return QString();
        }

        hiragana += convertKatakanaToHiragana(c);
    }

    return hiragana;
}

/**
 * Converts a katakana char to hiragana.
 * @param jTerm A katakana char to be made into hiragana
 * @return A hiragana version of the katakana char
 */
QChar Converter::convertKatakanaToHiragana(QChar c)
{
    return QChar(c.unicode() - kanaDifference);
}

/**
 * Determines if a string is composed entirely of roman characters
 * @param jTerm The string to be evaluated
 * @return Whether the string is entirely roman characters
 */
bool Converter::isRoman(QString jTerm)
{
    QRegularExpression re("^[\\w'-*\\.?\\\\]+$");
    QRegularExpressionMatch match = re.match(jTerm);

    return match.hasMatch();
}

/**
 * Determines if a string is composed entirely of kana; handles both hiragana and katakana.
 * @param jTerm The string to be evaluated
 * @return Whether the string is entirely kana
 */
bool Converter::isKana(QString jTerm)
{
    //Special cases
    if (jTerm.isEmpty()) {
        return false;
    }
    if (jTerm.size() == 1 && (isSpecialKana(jTerm) || jTerm == "ヽ")) {
        return false;
    }

    QStringList tokens = tokenizeKana(jTerm);

    for (int i = 0; i < tokens.size(); i++) {
        QString token = tokens.at(i);

        if (!(isHiragana(token) || isKatakana(token))) {
            return false;
        }
    }

    return true;
}

/**
 * Determines if a string is composed entirely of hiragana or acceptable regex.
 * @param jTerm The string to be evaluated
 * @return Whether the string is entirely hiragana
 */
bool Converter::isHiragana(QString jTerm)
{
    for (int i = 0; i < jTerm.size(); i++) {
        QChar c = jTerm.at(i);

        //char does not fall in the range of hiragana
        if (!(isHiragana(c) || isRegex(c) || isSpecialKana(c))) {
            return false;
        }
    }

    return true;
}

/**
 * Determines if a character is hiragana.
 * @param c The char to be evaluated
 * @return Whether the character is hiragana
 */
bool Converter::isHiragana(QChar c)
{
    return QString("ぁ") <= c && c <= QString("ゖ");
}

/**
 * Determines if a string is composed entirely of katakana or acceptable regex.
 * @param jTerm The string to be evaluated
 * @return Whether the string is entirely katakana
 */
bool Converter::isKatakana(QString jTerm)
{
    for (int i = 0; i < jTerm.size(); i++) {
        QChar c = jTerm.at(i);

        //char does not fall in the range of katakana
        //Should I include ヷ, ヸ, ヹ and ヺ?
        if (!(isKatakana(c) || isRegex(c) || isSpecialKana(c))) {
            return false;
        }
    }

    return true;
}

/**
 * Determines if a character is katakana.
 * @param c The char to be evaluated
 * @return Whether the character is katakana
 */
bool Converter::isKatakana(QChar c)
{
    return QString("ァ") <= c && c <= QString("ヶ");
}

/**
 * Determines if a string is composed entirely of special kana.
 * @param jTerm The string to be evaluated
 * @return Whether the string is entirely special kana
 */
bool Converter::isSpecialKana(QString jTerm)
{
    for (int i = 0; i < jTerm.size(); i++) {
        QChar c = jTerm.at(i);

        if (!isSpecialKana(c)) {
            return false;
        }
    }

    return true;
}

/**
 * Determines if a character is a special kana.
 * @param c The char to be evaluated
 * @return Whether the character is hiragana
 */
bool Converter::isSpecialKana(QChar c)
{
    return c == QString("ー") || c == QString("・");
}

/**
 * Determines if a string has any regex characters.
 * @param jTerm The string to be evaluated
 * @return Whether the string has regex characters
 */
bool Converter::hasRegex(QString jTerm)
{
    for (int i = 0; i < jTerm.size(); i++) {
        QChar c = jTerm.at(i);

        if (isRegex(c)) {
            return true;
        }
    }

    return false;
}

/**
 * Determines if a character is one used in regular expressions.
 * @param c The char to be evaluated
 * @return Whether the character is one used in regular expressions
 */
bool Converter::isRegex(QChar c)
{
    return (c == '.') || (c == '*') || (c == '?') || (c == '[') || (c == ']') ||
           (c == QString("＊")) || (c == QString("？")) || (c == QString("。"));
}
